function say(msg)
    print_with_color(:yellow, "NOTE: $msg\n", bold=true)
end

function plt(p)
    plot(p, width=1920, height=1080)
end

# Preloading of resources -----------------------------------------------------
if @isdefined(beij)
    @enableplots

    say("Loading Beijing")
    beij = load("data/osm/beijing-hw.osm.cbf");
    mmb = MapMatcher(beij, 1000.0);

    say("Plain Beijing plot")
    pbeij = osmplot(beij)

    say("Loading all matched paths")
    paths = load("data/paths/geolife-all-1604.pth.cbf")
end

W=5
gray=(.5,.5,.5)
green=(.0,1.,.0)
blue=(.0,.3,1.)
# -----------------------------------------------------------------------------

function demo1()
    say("Showing plain Beijing plot")
    plt(pbeij)
end

function demo2()
    tr = first(load("./data/g15-15.tr.cbf"))
    trp = preprocess(tr, 50.0)

    say("Printing trace GeoLife[15][15]:\n\t$tr")
    p = mmtrace(pbeij, tr, width=W)
    plt(p)

    say("Preprocessing trace GeoLife[15][15]:\n\t$trp")
    p = mmtrace!(p, trp, color=green, width=W)
    plt(p)

    say("Matching trace GeoLife[15][15]")
    conf = BssConfig(max_frame_size=50)
    trellis1 = bss_lattice(mmb, trp.positions, conf)
    println(trellis1)

    say("Stuck at observation $(trellis1.err_t)")
    p = mmpath(pbeij, mmb, get_best_path(trellis1), width=1.5*W, color=green)
    p = mmtrace!(p, trp, width=W, color=gray)
    p = mmtrace!(p, trp[trellis1.err_t-2:trellis1.err_t+2], width=2*W)
    plt(p)

    say("Matching the remaining part")
    trellis2 = bss_lattice(mmb, trp.positions[trellis1.err_t+3:end], conf)
    println(trellis2)
    p = mmpath(pbeij, mmb, get_best_path(trellis1), width=1.5*W, color=green)
    p = mmpath!(p, mmb, get_best_path(trellis2), width=1.5*W, color=blue)
    p = mmtrace!(p, trp, width=W, color=gray)
    p = mmtrace!(p, trp[trellis1.err_t-2:trellis1.err_t+2], width=2*W)
    plt(p)

    debug_print(trellis2)
end

function demo3()
    #say("Construction adjacency matrix")
    #A = mmmatrix(mmb, [collect(1:length(mmb.hmm_states))], true)

    say("Construction MM-matrix")
    M = mmmatrix(mmb, paths, true)

    #say("Computing exponential of A w/ naive method")
    #Aexp = exp(A)
    #centrA = centralities(Aexp)
    #boundrA = colorfn_boundaries(centrA, PerceptualColourMapsHEAT(), 0.0)

    say("Computing exponential of M w/ naive method")
    Mexp = exp(M)
    centrM = centralities(Mexp)
    boundrA = colorfn_boundaries(centrM, PerceptualColourMapsHEAT(), 0.0)

    #say("Building colored plot for A")
    #pA = osmplot(beij, colorfn=colorfn(centrA, boundaries=boundrA))
    #plt(pA)

    say("Building colored plot for M")
    pM = osmplot(beij, colorfn=colorfn(centrM, boundaries=boundrA))
    plt(pM)
end
