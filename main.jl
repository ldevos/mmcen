# vim: set filetype=julia

# author: Laurens Devos

const Float = Float64 # TODO clean Float usage!
const Prob = Float64


# MODULES -------------------------------------------------

_modules = [ :Dates,
             :Printf,
             :Random,
             :StaticArrays,
             :SparseArrays,
             :GeometryTypes,
             :CodecZstd,
             :Distributions,
             :ProgressMeter ]

for _module in _modules
    printstyled(string(_module), " ", color=:light_black)
    eval(:(import $_module))
end
println()


# SOURCE FILES --------------------------------------------

_includes = [ "util",
              "point",
              "hmm",
              "graph",
              "trace",
              "osm_map",
              "cbf",
              "load",
              "mm",
              "match",
              "matrix" ]

for _include in _includes
    printstyled(_include, " ", color=:light_black)
    include(joinpath(@__DIR__, "src", "$_include.jl"))
end
printstyled("Done\n", color=:green, bold=true)


# UTILITY MACROS ------------------------------------------

macro enablexml()
    :(include("src/osm_map_xml.jl")) # requires LibExpat
end

macro enablegeolife()
    :(include("src/geolife.jl")) # requires ZipFile
end

macro enableplots()
    :(include("src/glplot2.jl")) # requires GLFW, ModernGL
end

macro r(name)
    local fstr = string(name)
    local file = "src/" * fstr * ".jl"
    quote 
        printstyled("INFO: reloading '", color=:light_black)
        printstyled($file, bold=true, color=:light_black)
        printstyled("'\n", color=:light_black)
        include($file)
    end
end
