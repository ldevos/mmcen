# vim: set filetype=julia

# author: Laurens Devos
#
# Hidden markov models (HMM)
#
# REFERENCES:
#     [1]: Rabiner, Lawrence R. "A tutorial on hidden Markov models and selected
#     applications in speech recognition." Proceedings of the IEEE 77.2 (1989):
#     257-286.

abstract type HmmBase end

"""
    initial_prob(hmm, state)

Return the initial probability of the given state.
"""
function initial_prob(hmm::HmmBase, state::Int)::Prob
    error("Implement 'initial_prob' for $(typeof(hmm))")
end

"""
    transition_prob(hmm, state1, state2)

Return the probability of moving from state1 to state2 in the HMM.
"""
function transition_prob(hmm::HmmBase, state1::Int, state2::Int)::Prob
    error("Implement 'transition_prob' for $(typeof(hmm))")
end

"""
    observation_prob(hmm, state, observation)

Compute the probabability of observing the given observation in the given
state.
"""
function observation_prob(hmm::HmmBase, state::Int, observation)::Prob
    error("Implement 'observation_prob' for $(typeof(hmm)) ",
          "for observation type ", typeof(observation))
end

"""
    states(hmm)

Return the hidden states of the HMM.
"""
function states(hmm::HmmBase)
    error("Implement 'states' for $(typeof(hmm)) ")
end

"""
    states_near(hmm, observation)

Return an iterable that contains the states that are worth considering given
the observation.
"""
function states_near(hmm::HmmBase, observation)
    error("Implement 'states_near' for $(typeof(hmm)) ",
          "for observation type ", typeof(observation))
end

"""
    observation_between(hmm, prev_obs, obs)

Generate a likely observation between `o1` and `o2`. If it is unproductive to
add an additional observation between `o1` and `o2`, return `o2`. `OT` refers
to observation type.
"""
function observation_between(hmm::HmmBase, o1::OT, o2::OT)::OT where OT
    error("Implement 'observation_between' for $(typeof(hmm)) ",
          "for observation type ", OT)
end


###############################################################################


"""
    nb_states(hmm)

Get the number of states in the HMM.
"""
function nb_states(hmm::HmmBase)::Int
    length(states(hmm))
end

"""
    transition_prob_matrix(hmm)

Get the full transition matrix.
"""
function transition_prob_matrix(hmm::HmmBase)::Matrix{Prob}
    N = nb_states(hmm)
    matrix = Matrix{Prob}(N, N)
    for i in states(hmm), j in states(hmm)
        matrix[i, j] = transition_prob(hmm, i, j)
    end
    matrix
end

"""
    prob_of_states(hmm, states)

Given an iterable of states, compute the probability of the given state
sequence.
"""
function prob_of_states(hmm::HmmBase, states)::Prob
    iter_state = start(states)

    done(states, iter_state) && return 1.0
    state0, iter_state = next(states, iter_state)

    prob = initial_prob(hmm, state0)

    while !done(states, iter_state)
        state1, iter_state = next(states, iter_state)
        prob *= transition_prob(hmm, state0, state1)
        state0 = state1
    end

    prob
end

struct ProbOfObservationsResult
    alpha::Vector{Tuple{Int, Prob}}
    frames::Vector{Int}
end

"""
    prob_of_observations(hmm, observations)

Given an iterable of observations, compute the probability of observing the
given sequence of observations.

We compute the probability:
```math
P(O_1,\\ldots,O_T \\mid \\lambda)
```

Reference: [1] p262, the Forward-Backward procedure
"""
function prob_of_observations(hmm::HmmBase, observations)::ProbOfObservationsResult
    iter_state = start(observations)

    done(observations, iter_state) && return one(Prob)
    obs, iter_state = next(observations, iter_state)

    alpha = Tuple{Int, Prob}[] # [(state, α_t(state))]

    # initialize for t=1
    for state in states_near(hmm, obs)
        state_prob = initial_prob(hmm, state) * observation_prob(hmm, state, obs)
        push!(alpha, (state, state_prob))
    end

    frames = Int[1, length(alpha)+1] # contains the index of the first
                                      # (state, prob) pair for each t
                                      # we initialize with 1 (for t=1)
                                      # and length(α)+1 for t=2
    # recur t->t+1
    while !done(observations, iter_state)
        obs, iter_state = next(observations, iter_state)
        prev_states = view(alpha, frames[end-1]:frames[end]-1)
        isempty(prev_states) && error("no more candidates")
        for st1 in states_near(hmm, obs)
            st1_prob = zero(Prob)
            for (st0, st0_prob) in prev_states
                st0_to_st1_prob = transition_prob(hmm, st0, st1) * st0_prob
                st1_prob += st0_to_st1_prob
            end
            st1_prob *= observation_prob(hmm, st1, obs)
            st1_prob > zero(Prob) && push!(alpha, (st1, st1_prob))
        end
        push!(frames, length(alpha)+1)
    end

    ProbOfObservationsResult(alpha, frames)
end

function Base.get(res::ProbOfObservationsResult)
    sum(last, res.alpha[res.frames[end-1]:res.frames[end]-1])
end

function debug_print(io::IO, res::ProbOfObservationsResult)
    frames = res.frames
    alpha = res.alpha
    for (t, (lo, hi)) in enumerate(zip(frames[1:end-1], frames[2:end]))
        println(io, "== (lo, hi) = ($lo, $hi) =========")
        for (st, st_prob) in alpha[lo:hi-1]
            Printf.@printf(io, "α%d(%d) = %s\n", t, st, string(st_prob))
        end
    end
    println(io, "==============================")
end
debug_print(res::ProbOfObservationsResult) = debug_print(STDOUT, res)



## BSS = Best State Sequence ##################################################


"""
Status values for `BssLattice`.
"""
@enum(BssStatus,
    BSS_SUCCESS,
    BSS_OUT_OF_TIME,
    BSS_EMPTY_FRAME)

@enum(BssPrint,
    BSS_PRINT_NOTHING,
    BSS_PRINT_PROGRESS,
    BSS_PRINT_DEBUG)

"""
Element of the best state sequence lattice. Contains the matched HMM state, a
pointer back to the previous element and the probability of this matching.
"""
struct BssMatching
    hmm_state::Int      # Identifier of the HMM state.
    prev::Int           # Index in `matchings` of previous best matching.
    prob::Prob          # Normalized probability.
end

function BssMatching(h, δ, p)
    BssMatching(convert(Int, h), convert(Int, δ), convert(Prob, p))
end

function normalize(m::BssMatching, summed_prob::Prob)
    BssMatching(m.hmm_state, m.prev, m.prob/summed_prob)
end

const BssMatchings = Vector{BssMatching}

# BSS configuration
struct BssConfig
    nest_enabled::Bool       # Enable/disable non-emitting states
    max_frame_size::Int      # Pruning, a value of 0 disabled pruning
    print::BssPrint          # Print a progress bar
    max_runtime_ms::Int      # Runtime in milliseconds
    ratio_threshold::Float64
end

function BssConfig(; nest_enabled::Bool=true,
                     max_frame_size::Int=200,
                     print::BssPrint=BSS_PRINT_PROGRESS,
                     max_runtime_ms::Int=10000,
                     ratio_threshold::Float64=2.0)
    BssConfig(nest_enabled, max_frame_size, print, max_runtime_ms,
              ratio_threshold)
end

function BssConfig(s::Symbol)
    if s == :prune200 || s == :prune
        BssConfig(max_frame_size=200)
    else
        BssConfig()
    end
end

"""
The lattice that is constructed by the `best_state_seq` function (see Viterbi
algorithm [1]).

All elements in a frame are matched to the same observation.

Type parameter `OT` (Observation Type) is the type of the observations. This
type can vary depending on the HMM implementation.
"""
mutable struct BssLattice{OT}
    matchings::BssMatchings     # Vector containing all observation-to-hmm_state matchings
    frames::Vector{Int}         # Indices into `matchings` delimiting frames.
    observations::Vector{OT}    # Observation that was matched for each frame.
    summed_probs::Vector{Prob}  # Frame probability normalizers
    conf::BssConfig
    nest_max_depth::Int         # Max number of consecutive NESTs
    nb_nest::Int                # N° of inserted non-emitted states (NESTs).
    nb_discarded_frames::Int
    nb_discarded_matchings::Int
    nb_empty_frames::Int        # Number of times a frame of size 0 is encountered.
    nb_pruned_matchings::Int
    time::Dates.Millisecond     # Amount of time it took to construct the lattice.
    status::BssStatus           # Success, or a reason for the failure.
    err_t::Int                  # Index of observation that caused non-success status
end

""" Construct new lattice. """
function BssLattice(::Type{OT}, conf::BssConfig) where {OT}
    matchings = BssMatching[]
    frames = Int[1]             # `frames[end]` = first index of the next frame to come.
    observations = OT[]         # inv: `length(frames)-1 == length(observations)`
    summed_probs = Prob[]       # inv: `length(observations)` == length(summed_probs)`

    BssLattice{OT}(matchings, frames, observations, summed_probs, conf,
                   0, 0, 0, 0, 0, 0, Dates.Millisecond(0),
                   BSS_SUCCESS, 0)
end

Base.length(l::BssLattice{OT}) where {OT} = max(0, length(l.frames)-1)
Base.show(io::IO, l::BssLattice{OT}) where {OT} = begin
    mean_frame_size, min_frame_size, max_frame_size = begin
        local Δ = diff(l.frames)
        isempty(Δ) ? (0,0,0) : (round(Int,mean(Δ)), minimum(Δ), maximum(Δ))
    end

    println(io, typeof(l), " with ", l.conf)
    println(io, " - #matchings    ", length(l.matchings))
    println(io, " - #frames       ", length(l.frames)-1)
    println(io, " - #nest         ", l.nb_nest, ", max depth of ", l.nest_max_depth)
    println(io, " - #discarded    ", l.nb_discarded_frames, " frames and ",
                                     l.nb_discarded_matchings, " matchings")
    println(io, " - #pruned       ", l.nb_pruned_matchings)
    println(io, " - time          ", l.time)
    println(io, " - rate          ±", div((length(l.matchings) + l.nb_discarded_matchings) * 1000,
                                          Dates.value(l.time)+1), " matchings per second")
    println(io, " - frame stats   mean=$mean_frame_size min=$min_frame_size max=$max_frame_size",
                                  " #empty frames=$(l.nb_empty_frames)")
    print(  io, " - status        ")
    print_with_color(l.status == BSS_SUCCESS ? :green : :red,
                     io, l.status,
                     l.err_t == 0 ? "" : " (at obs $(l.err_t))")
end

# -- HELPER FUNCTIONS FOR BSS -------------------------------------------------


@inline function _push_matching!(matchings::BssMatchings, hmm_state, prev, prob)
    push!(matchings, BssMatching(hmm_state, prev, prob))
end

# after having pushed matchings, close the frame by pushing a frame index
function _push_frame!(lattice::BssLattice{OT}, obs::OT) where {OT}
    max_frame_size = lattice.conf.max_frame_size
    frame_start = lattice.frames[end]
    frame_end = length(lattice.matchings)

    # PRUNING (if enabled, i.e. max_frame_size > 0):
    if max_frame_size > 0
        sort!(view(lattice.matchings, frame_start:frame_end); by=m->m.prob, rev=true)

        frame_size = frame_end - frame_start + 1
        if frame_size > max_frame_size
            frame_end = frame_start + max_frame_size - 1
            resize!(lattice.matchings, frame_end)
            lattice.nb_pruned_matchings += frame_size - max_frame_size
        end
    end

    # NORMALIZE FRAME:
    summed_prob = begin
        S, ms = zero(Prob), lattice.matchings
        for i in frame_start:frame_end; S += ms[i].prob; end
        for i in frame_start:frame_end; ms[i] = normalize(ms[i], S); end
        S
    end

    # PUSH FRAME INFORMATION:
    push!(lattice.frames, frame_end+1)          # Push first index of next frame.
    push!(lattice.observations, obs)            # Push observation associated with this frame.
    push!(lattice.summed_probs, summed_prob)    # Store normalizer factor.
end

# get the indices of the last frame on the lattice;
# rev_index==-1 => last frame on lattice
# rev_index==-2 => second to last frame on lattice
@inline function _last_frame_indices(lattice::BssLattice{OT}, rev_index::Int) where {OT}
    lattice.frames[end+rev_index]:lattice.frames[end+rev_index+1]-1
end

# Push the first frame, there is no previous frame to compare to.
function _push_initial_frame!(hmm::HmmBase, lattice::BssLattice{OT}, obs::OT) where {OT}
    for st1 in states_near(hmm, obs)
        st1_prob = initial_prob(hmm, st1) * observation_prob(hmm, st1, obs)
        _push_matching!(lattice.matchings, st1, 0, st1_prob) # invalid prev
    end
    _push_frame!(lattice, obs)
end

# For each state near the given `obs`, look at the best
function _construct_recur_frame(hmm::HmmBase, lattice::BssLattice{OT},
                                prev_frame_indices::UnitRange{Int64},
                                matchings::BssMatchings, obs::OT) where {OT}
    @assert !isempty(prev_frame_indices)

    for st1 in states_near(hmm, obs)        # For each state near obs...
        max_st0_to_st1_prob, max_st0_idx = zero(Prob), 0
        for idx in prev_frame_indices       # ...look for the best previous matching.
            st0, st0_prob = begin           # Take `hmm_state` and `prob` from matching.
                m = lattice.matchings[idx]
                convert(Int64, m.hmm_state), m.prob
            end
            st0_to_st1_prob = transition_prob(hmm, st0, st1) * st0_prob
            if st0_to_st1_prob > max_st0_to_st1_prob   # Find matching w/ max prob...
                max_st0_to_st1_prob = st0_to_st1_prob  # ...and keep track of index.
                max_st0_idx = idx

                # Frames are sorted when pruning is enabled, use this property!
                # We won't find a better previous state.
                lattice.conf.max_frame_size > 0 && break
            end
        end
        if max_st0_to_st1_prob > zero(Prob) # Only consider possible states.
            st1_prob = max_st0_to_st1_prob * observation_prob(hmm, st1, obs)
            _push_matching!(matchings, st1, max_st0_idx, st1_prob)
        end
    end
end

# Push new frame for `obs` connecting from last frame directly to `lattice`
function _push_recur_frame!(hmm::HmmBase, lattice::BssLattice{OT}, obs::OT) where {OT}
    prev_frame_indices = _last_frame_indices(lattice, -1)
    _construct_recur_frame(hmm, lattice, prev_frame_indices,
                           lattice.matchings, obs)
    _push_frame!(lattice, obs)
end

# Push frame in `tmp_matchings` to the lattice
function _push_tmp_frame!(lattice::BssLattice{OT}, tmp_matchings::BssMatchings,
                          obs::OT) where {OT}
    append!(lattice.matchings, tmp_matchings)
    empty!(tmp_matchings)
    _push_frame!(lattice, obs)
end

# Remove the last frame on `lattice`
function _discard_last_frame!(lattice::BssLattice{OT}) where {OT}
    indices = _last_frame_indices(lattice, -1)
    lattice.nb_discarded_frames += 1
    lattice.nb_discarded_matchings += length(indices)

    pop!(lattice.frames)
    pop!(lattice.observations)
    pop!(lattice.summed_probs)
    resize!(lattice.matchings, first(indices)-1)
end

# Just emptying the passed `tmp_matchings` vector + statistics
function _discard_tmp_matchings!(lattice::BssLattice{OT},
                                 tmp_matchings::BssMatchings) where {OT}
    if !isempty(tmp_matchings)
        lattice.nb_discarded_frames += 1
        lattice.nb_discarded_matchings += length(tmp_matchings)
        empty!(tmp_matchings)
    end
end

@inline function _summed_prob_ratio(summed_prob1::Prob, summed_prob2::Prob)
    summed_prob2==zero(Prob) ? Inf : summed_prob1 / summed_prob2
end


# -- LATTICE CONSTRUCTION -----------------------------------------------------

macro _break_with_status(lattice, status, t)
    quote
        $(esc(lattice)).status = $(esc(status))
        $(esc(lattice)).err_t = $(esc(t))
        break
    end
end

@inline function _debug_print(t, lattice, obs_stack, ratio)
    lattice.conf.print != BSS_PRINT_DEBUG && return

    Printf.@printf("t=%d sp=%.2e d=%d ", t, lattice.summed_probs[end],
                 length(obs_stack))

    s = Printf.@sprintf("r=%.2e", ratio)
    print_with_color(ratio>lattice.conf.ratio_threshold ? :yellow : :default, s)

    Printf.@printf(" sz=%-3d ", length(_last_frame_indices(lattice, -1)))

    # NO NEST, top frame made permanent, obs_stack popped
    if isempty(obs_stack) || lattice.observations[end] != obs_stack[end]
        Printf.@printf("%s ✓\n", lattice.observations[end])

    # last frame removed, NEST pushed, deciding on NEST next iteration
    else
        Printf.@printf("%s before %s (%.2e)\n",
                lattice.observations[end], obs_stack[end-1],
                norm(lattice.observations[end]-obs_stack[end-1]))
    end
end

"""
Construct the lattice for Best State Sequence (BSS).
TODO docs
"""
function bss_lattice(hmm::HmmBase, observations::Vector{OT},
                     conf::BssConfig) where {OT}
    observations = remove_consecutive_duplicates(observations) # Avoid issues w/ equality of consec. obs
    lattice = BssLattice(OT, conf)

    T = length(observations); T == 0 && return lattice
    start_time = time()
    time_limit = start_time + (conf.max_runtime_ms / 1000.0)

    if conf.print == BSS_PRINT_PROGRESS
        meter = ProgressMeter.Progress(T; dt=0.1, desc="", barglyphs=ProgressMeter.BarGlyphs("[=> ]"))
    end

    # Define data structures consecutive
    tmp_matchings = BssMatching[]
    obs_stack = OT[]

    # Match first observation
    _push_initial_frame!(hmm, lattice, observations[1])
    t = 1 # last observation that was matched

    while true
        time() > time_limit && @_break_with_status(lattice, BSS_OUT_OF_TIME, t)

        if isempty(obs_stack)   # Take the next obs to match from `observations`
            if t < T
                push!(obs_stack, observations[t += 1])
                conf.print==BSS_PRINT_PROGRESS && ProgressMeter.update!(meter, t)
            else break end      # We're done, all observations matched
        end

        lattice.nb_empty_frames += isempty(_last_frame_indices(lattice, -1))

        # The observation at the end of the `obs_stack` is the observation to
        # be considered in this iteration. If the last frame on `lattice`
        # already concerns this observation (was built in a prev. iteration),
        # then just continue, else push a new last frame for `obs_stack[end]`
        # (current last frame is permanent and won't change anymore).
        if lattice.observations[end] != obs_stack[end]
            if isempty(_last_frame_indices(lattice, -1))         # The current last frame is permanent;
                _discard_last_frame!(lattice)                    # it must be non-empty otherwise we
                @_break_with_status(lattice, BSS_EMPTY_FRAME, t) # can't build on it.
            end
            _push_recur_frame!(hmm, lattice, obs_stack[end])     # (!) possibly empty frame
        end

        @assert lattice.observations[end] == obs_stack[end]
        @assert isempty(tmp_matchings)

        nest_obs, ratio = obs_stack[end], 0.0 # Overwritten when used, dummy values!

        # Next we decide whether to keep the last frame, or replace it with a
        # NEST and consider the current `obs_stack[end]` later (by leaving it
        # on `obs_stack`
        if conf.nest_enabled
            # Ask specific HMM implementation to generate an observation that
            # lies between the last two matched observations.
            nest_obs = observation_between(hmm, lattice.observations[end-1],
                                                lattice.observations[end])

            if nest_obs != lattice.observations[end]
                # Construct a temporary frame for this observation connecting from the
                # second-to-last frame.
                prev_frame_indices = _last_frame_indices(lattice, -2)
                _construct_recur_frame(hmm, lattice, prev_frame_indices,
                                       tmp_matchings, nest_obs)
                summed_prob_nest = mapreduce(x -> x.prob, +, zero(Prob), tmp_matchings)

                # See how this temporary frame performs compared to the last frame.
                ratio = _summed_prob_ratio(summed_prob_nest, lattice.summed_probs[end])
            end
        end

        if ratio > conf.ratio_threshold
            _discard_last_frame!(lattice)
            _push_tmp_frame!(lattice, tmp_matchings, nest_obs)
            push!(obs_stack, nest_obs)
            lattice.nb_nest += 1
            lattice.nest_max_depth = max(lattice.nest_max_depth, length(obs_stack)-1)
        else
            _discard_tmp_matchings!(lattice, tmp_matchings)
            pop!(obs_stack) # Accept last frame as permanent
        end

        _debug_print(t, lattice, obs_stack, ratio)
    end

    if conf.print==BSS_PRINT_PROGRESS && lattice.status != BSS_SUCCESS
        ProgressMeter.cancel(meter)
    end

    lattice.time = Dates.Millisecond(round(Int, (time()-start_time)*1000))
    lattice
end



# -- FUNCTIONS FOR `BssLattice` -----------------------------------------------

"""
Return the state sequence such that the following probability is maximized:
```math
P(Q_1, \\ldots, Q_T \\mid O_1,\\ldots,O_T, \\lambda)
```

Reference: [1], p264, the Viterbi algorithm
"""
function get_best_path(lattice::BssLattice, remove_duplicates::Bool=true)
    ms = lattice.matchings
    fs = lattice.frames
    path = Int[]

    # Find last node with max probability.
    last_frame = @view ms[fs[end-1]:fs[end]-1]
    matching = reduce((m1, m2) -> m1.prob>m2.prob ? m1 : m2, BssMatching(0,0,0.0), last_frame)
    matching.hmm_state == 0 && error("no path exists")

    # Loop through prev states until prev becomes 0 (invalid)
    while true
        push!(path, matching.hmm_state)
        matching.prev == 0 && break
        matching = ms[matching.prev]
    end

    reverse!(path)
    remove_duplicates ? remove_consecutive_duplicates(path) : path
end

"""
Generate a random path using the probability distribution in the given lattice.
"""
function get_random_path(hmm::HmmBase,
                         lattice::BssLattice,
                         remove_duplicates::Bool=true,
                         rng::Random.AbstractRNG=Random.MersenneTwister(0))
    ms = lattice.matchings
    fs = lattice.frames
    N = length(fs)-1
    path = Vector{Int}(N)

    for t in N:-1:1
        matchings = sort(filter(ms[fs[t]:fs[t+1]-1]) do m
            t==N || transition_prob(hmm, m.hmm_state, path[t+1]) > zero(Prob)
        end; by=m->m.prob, rev=true)

        probs = map(matchings) do m
            m.prob * (t==N ? 1.0 : transition_prob(hmm, m.hmm_state, path[t+1]))
        end
        probs ./= sum(probs) # normalize
        cumprobs = cumsum(probs)

        isempty(cumprobs) && error("invalid lattice: stuck at $t")

        i = searchsortedfirst(cumprobs, rand(rng))
        path[t] = matchings[i].hmm_state
    end

    remove_duplicates ? remove_consecutive_duplicates(path) : path
end

"""
Generate `K` random paths with `get_random_path`.
"""
function get_random_paths(hmm::HmmBase,
                          lattice::BssLattice, K::Int,
                          remove_duplicates::Bool=true,
                          rng::Random.AbstractRNG=Random.MersenneTwister(0))
    paths = Vector{Vector{Int}}(K)
    for i in 1:K
        paths[i] = get_random_path(hmm, lattice, remove_duplicates, rng)
    end
    paths
end

"""
The resulting array containing the most likely HMM states given the
observation sequence can contain duplicates:
```
    [ 1 1 1 2 3 4 4 5 ] => [ 1 2 3 4 5 ]
```
"""
function remove_consecutive_duplicates(arr)
    isempty(arr) && return arr
    nodupl = eltype(arr)[arr[1]]
    for st in @view(arr[2:end])
        nodupl[end] != st && push!(nodupl, st)
    end
    nodupl
end

"""
Print the lattice in a readable format.
"""
function debug_print(io::IO, lattice::BssLattice{OT}) where {OT}
    matchings = lattice.matchings
    frames = lattice.frames
    path = try get_best_path(lattice, false) catch e Int[] end
    K = length(path)

    for (t, (lo, hi)) in enumerate(zip(frames[1:end-1], frames[2:end]))
        frame_header = Printf.@sprintf(
                    "\n== frame[%d:%d], width=%d, summed_prob=%.2e, obs=%s ====\n",
                    lo, hi-1, hi-lo, lattice.summed_probs[t], lattice.observations[t])
        print_with_color(:bold, io, frame_header)

        best = reduce((p,δ)-> max(p, δ.prob), zero(Prob), matchings[lo:hi-1])
        for idx in lo:hi-1
            m = matchings[idx]
            mprev = m.prev == 0 ? BssMatching(0, 0, 0.0) : matchings[m.prev]
            s = Printf.@sprintf("%s %-4d: δ%d(%d) = %.5e from %d (%d)\n", t < K
                                && m.hmm_state≈path[t] ? "✓" : " ", idx, t,
                                m.hmm_state, m.prob, mprev.hmm_state, m.prev)
            if m.prob ≈ best print_with_color(:green, io, s; bold=true)
            else print(io, s) end
        end
    end

    uniqpath = remove_consecutive_duplicates(path)
    Ku = length(uniqpath)
    b = IOBuffer()
    isempty(uniqpath) && print(b, "<no path>")
    for (i, st) in enumerate(uniqpath)
        if i == 1                    print(b, st)
        elseif length(uniqpath) < 10 print(b, ", ", st)
        else
            if i<5 || i>Ku-5         print(b, ", ", st)
            elseif i == 5            print(b, ", … ")
            end
        end
    end

    println(io)
    println(io, "==============================")
    show(io, lattice)
    println(io)
    println(io, " - path          ", String(take!(b)))
    println(io, " - path stats    #path=$K #path.nodupl=$Ku #dupl=$(K-Ku)")
    println(io, "==============================")
end
debug_print(lattice::BssLattice) = debug_print(STDOUT, lattice)

