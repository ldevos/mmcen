function _parse_machinefile(file)
    open(file, "r") do f
        ls = filter(l->!isempty(l)&&!startswith(l,"#"), readlines(f))
        machines = map(l->(l, :auto), ls)
        machines
    end
end

function setup_cluster()
    machines = _parse_machinefile("machinefile")
    ids = addprocs(machines; tunnel=true, topology=:master_slave)
    cluster_reload()
    ids
end

function cluster_reload()
    @everywhere begin
        if myid() != 1
            include("./main.jl")
            global beij = load("data/osm/beijing-hw.osm.cbf")
            global mmb = MapMatcher(beij, 1000.0)
            global conf = BssConfig(print=BSS_PRINT_NOTHING)
            global HOSTNAME = chomp(readstring(`hostname`))
            println("Worker $HOSTNAME:$(myid()) initialized")
        end
    end
end

function stop_cluster(pids)
    Base.Distributed.interrupt(pids)
end


# -- CLUSTERED FUNCTIONS ------------------------------------------------------

function _test_clustered_f(r::Float64)
    println("zZz $HOSTNAME:$(myid()) $(round(r, 2))", isdefined(:mmb) ? " mmb ✓" : "")
    sleep(r)
end

function test_clustered()
    np = nprocs()
    i = 0
    N = 100

    rnd = MersenneTwister(0)
    rs = rand(rnd, N)

    @sync for p in 1:np
        p == myid() && np != 1 && continue
        @async begin
            while i < N
                myi = (i+=1)
                print_with_color(:yellow, "OUT:  Sending $myi to $p\n")

                remotecall_fetch(_test_clustered_f, p, rs[i])

                print_with_color(:yellow, "DONE: Completed $myi at $p\n")
            end
        end
    end
end

function export_geolife_cbf_clustered(geolife_zip::String, osmmap::OsmMap, group_start=1)
    np = nprocs()
    i = group_start-1
    N = open(GeoLife, geolife_zip) do gl length(gl.groups) end

    @sync for p in 1:np
        p == myid() && np != 1 && continue
        @async begin
            while i < N
                myi = (i+=1)
                print_with_color(:yellow, "OUT:  Sending $myi to $p\n")

                # we're sending osmmap, could be slow!
                remotecall_fetch(export_geolife_cbf, p, geolife_zip, "data/traces", osmmap, myi:myi)

                print_with_color(:yellow, "DONE: Completed $myi at $p\n")
            end
        end
    end
end

function _match_all_geolife_traces_clustered_f(tracename::String,
                                               traces::Vector{TraceXY})
    tic()
    println("Starting $tracename on $HOSTNAME:$(myid())")
    match_all(mmb, tracename, traces, conf) # mmb and conf are global variables!
    println("Done w/ $tracename in $(round(toq(), 2))s on $HOSTNAME:$(myid())")
end

function match_all_geolife_traces_clustered(tracefiles::Vector{String})
    np = nprocs()
    i = 0
    N = length(tracefiles)

    @sync for p in 1:np
        p == myid() && np != 1 && continue
        @async begin
            while i < N
                myi = (i+=1)
                traces = load(tracefiles[myi])
                tracename = filename(tracefiles[myi])

                print_with_color(:yellow, "OUT:  Sending $myi ($tracename) to $p\n")

                # avoid sending local beijing and conf to workers!
                #remotecall_fetch(match_all, p, mm, tracename, traces, conf)
                remotecall_fetch(_match_all_geolife_traces_clustered_f, p,
                                 tracename, traces)

                print_with_color(:yellow, "DONE: Completed $myi at $p\n")
            end
        end
    end
end
