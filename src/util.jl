# vim: set filetype=julia

# author: Laurens Devos

"""
    The radii of planet Earth.

ref: https://en.wikipedia.org/wiki/Earth_radius#Mean_radius
"""
@inline earth_mean_radius()       = 6371.0e3 #km
@inline earth_equatorial_radius() = 6378.1e3 #km
@inline earth_polar_radius()      = 6356.8e3 #km

function geolife_trace_files()
    dir = joinpath(@__DIR__, "../data/traces/")
    isgeolife = n -> startswith(n, "geolife") && endswith(n, ".tr.cbf")
    fullpath = n -> joinpath(dir, n)
    names = map(fullpath, filter(isgeolife, readdir(dir)))
    names
end

function filename(path::String, remove_extension=true)
    file = basename(path)
    if remove_extension
        spl = split(file, ".")
        !isempty(spl) ? String(spl[1]) : file
    end
end

function geolife_path_files()
    dir = joinpath(@__DIR__, "../data/paths/")
    files = readdir(dir)
    files = map(f -> joinpath(dir, f), filter(files) do f
        startswith(f, "geolife") && endswith(f, "pth.cbf")
    end)
    files
end
