# vim: set filetype=julia

# author: Laurens Devos

""" The vertex type of a `SimpleGraph` """
struct SimpleGraphVertex{N}
    id::Int
    incoming::Vector{Int}
    outgoing::Vector{Int}
end

SimpleGraphVertex{N}(id::Int) where {N} = SimpleGraphVertex{N}(id, Int[], Int[])

"""
An edge-list graph whose vertices hold data. Type `N` must be usable as a
key in a hash table.
"""
struct SimpleGraph{N}
    _vertex_list::Vector{N} # _vertices[_vertex_list[k]].id == k
    _vertices::Dict{N, SimpleGraphVertex{N}}
end

SimpleGraph{N}() where {N} = SimpleGraph{N}(N[], Dict{N, SimpleGraphVertex{N}}())

function hasnode(g::SimpleGraph{N}, n::N) where {N}
    haskey(g._vertices, n)
end

function hasedge(g::SimpleGraph{N}, n::N, m::N) where {N}
    hasnode(g, n) && hasnode(g, m) && g._vertices[m].id ∈ g._vertices[n].outgoing
end

function addnode!(g::SimpleGraph{N}, n::N) where {N}
    hasnode(g, n) && error("Node already in graph")
    v = SimpleGraphVertex{N}(length(g._vertex_list)+1)
    push!(g._vertex_list, n)
    g._vertices[n] = v
end

function addedge!(g::SimpleGraph{N}, n::N, m::N) where {N}
    hasnode(g, n) || error("Node n not in graph")
    hasnode(g, m) || error("Node m not in graph")

    m ∈ g._vertices[n].outgoing && error("Edge (n,m) already in graph")
    n ∈ g._vertices[m].incoming && error("Edge (n,m) already in graph")

    push!(g._vertices[n].outgoing, g._vertices[m].id)
    n != m && push!(g._vertices[m].incoming, g._vertices[n].id)

    nothing
end

function incoming(g::SimpleGraph{N}, n::N) where {N}
    hasnode(g, n) || error("Node n not in graph")
    map(i -> g._vertex_list[i], g._vertices[n].incoming)
end

function outgoing(g::SimpleGraph{N}, n::N) where {N}
    hasnode(g, n) || error("Node n not in graph")
    map(i -> g._vertex_list[i], g._vertices[n].outgoing)
end

function outgoing!(g::SimpleGraph{N}, n::N, dest::Vector{N}) where {N}
    outs = g._vertices[n].outgoing
    for x in outs
        push!(dest, g._vertex_list[x])
    end
end

function indegree(g::SimpleGraph{N}, n::N) where {N}
    hasnode(g, n) || error("Node n not in graph")
    length(g._vertices[n].incoming)
end

function outdegree(g::SimpleGraph{N}, n::N) where {N}
    hasnode(g, n) || error("Node n not in graph")
    length(g._vertices[n].outgoing)
end

function nodes(g::SimpleGraph{N}) where {N}
    copy(g._vertex_list)
end

function edges(g::SimpleGraph{N}) where {N}
    result = Tuple{N, N}[]
    for n in g._vertex_list, i in g._vertices[n].outgoing
        m = g._vertex_list[i]
        push!(result, (n, m))
    end
    result
end

function edge_indices(g::SimpleGraph{N}) where {N} # indices into `nodes(g)`
    result = Coord2[]
    for (i, n) in enumerate(g._vertex_list), j in g._vertices[n].outgoing
        push!(result, coord(i, j))
    end
    result
end

nb_nodes(g::SimpleGraph{N}) where {N} = length(g._vertex_list)
nb_edges(g::SimpleGraph{N}) where {N} = sum(n -> length(g._vertices[n].outgoing), g._vertex_list)

Base.size(g::SimpleGraph{N}) where {N} = (nb_nodes(g), nb_edges(g))

function Base.show(io::IO, g::SimpleGraph) # pretty printing
    nb_nodes = length(g._vertex_list)
    edgs = edges(g)
    nb_edges = length(edgs)

    print(io, typeof(g))
    println(io, "($nb_nodes, $nb_edges) {")
    for (i, (n, m)) in enumerate(edgs)
        if i < 50 || i == nb_edges - 1
            println(io, "    $n -> $m")
        elseif i == 50
            println(io, "    ...")
        end
    end
    print(io, "}")
end
