# vim: set filetype=julia

# author: Laurens Devos

function load(file)
    m = nothing

    if @isdefined(LibExpat) && @isdefined(parse_osm)
        ## STREAMING PBF->OSM XML
        # Julia 0.6.1 contains a bug:
        # https://discourse.julialang.org/t/freeze-when-reading-from-output-of-external-command/9034/4?u=jeffersyno
        # streaming solution sometimes freezes:
        if endswith(file, ".osm.pbf")
            #open(`osmium cat $file -f osm`, "r") do io
            #    m = parse_osm(io) # parse stream insead of string
            #end

            # read in string buffer first; streaming requires LibExpat patch
            m = parse_osm(`osmium cat $file -f osm`)
        end

        ## PLAIN XML
        if endswith(file, ".osm")
            m = parse_osm(file)
        end
    end

    # Custom binary format for OSM files
    if endswith(file, ".osm.cbf")
        open(file, "r") do f
            m = read_cbf(OsmMap, f)
        end
    end

    # Custom binary format for trace lists
    if endswith(file, ".tr.cbf")
        m = loadtraces(file)
    end

    # Custom binary format for path lists
    if endswith(file, ".pth.cbf")
        m = loadpaths(file)
    end

    if m == nothing
        error("Cannot determine file format of '$file' (@enablexml?)")
    end

    m
end

loadtraces(file) = open(file, "r") do f
    read_cbf(Vector{TraceXY}, f)
end

loadpaths(file) = open(file, "r") do f
    read_cbf(Vector{Vector{Int}}, f)
end
