# vim: set filetype=julia

# author: Laurens Devos
#
# Reader for Microsoft GeoLife dataset

if @isdefined(InfoZIP)
    printstyled("InfoZIP... ", color=:light_black)
    import InfoZIP

    printstyled("Done\n", bold=true, color=:green)
end

geolife_reftime() = DateTime(1899, 12, 30)

@enum(GeoLifeMode,
      GEOLIFE_WALK,
      GEOLIFE_BIKE,
      GEOLIFE_BUS,
      GEOLIFE_CAR,
      GEOLIFE_TAXI,
      GEOLIFE_SUBWAY,
      GEOLIFE_TRAIN,
      GEOLIFE_AIRPLANE,
      GEOLIFE_BOAT,
      GEOLIFE_RUN,
      GEOLIFE_MOTORCYCLE,
      GEOLIFE_UNKNOWN)

GEOLIFE_MODE_MAP = Dict{String, GeoLifeMode}(
    "walk"       => GEOLIFE_WALK,
    "bike"       => GEOLIFE_BIKE,
    "bus"        => GEOLIFE_BUS,
    "car"        => GEOLIFE_CAR,
    "taxi"       => GEOLIFE_TAXI,
    "subway"     => GEOLIFE_SUBWAY,
    "train"      => GEOLIFE_TRAIN,
    "airplane"   => GEOLIFE_AIRPLANE,
    "boat"       => GEOLIFE_BOAT,
    "run"        => GEOLIFE_RUN,
    "motorcycle" => GEOLIFE_MOTORCYCLE)

struct GeoLifeLabel
    start_time::DateTime
    end_time::DateTime
    mode::GeoLifeMode
end

struct GeoLifeGroup
    z::InfoZIP.Archive
    tracefiles::Vector{String}
    labelfile::String
end

struct GeoLife
    z::InfoZIP.Archive
    groups::Vector{GeoLifeGroup}
end

function GeoLife(file::String)
    z = InfoZIP.open_zip(file)

    groups = GeoLifeGroup[]
    tracefiles = String[]
    labelfile = ""
    groupid = 1

    for path in keys(z)
        pieces = split(path, "/")
        K = length(pieces)

        K < 4 && continue

        new_groupid = parse(Int, pieces[3]) + 1
        if new_groupid != groupid
            push!(groups, GeoLifeGroup(z, copy(tracefiles), labelfile))
            empty!(tracefiles)
            labelfile = ""
            groupid = new_groupid
        end

        if pieces[end] == "labels.txt"
            labelfile = path
        elseif endswith(pieces[end], ".plt")
            push!(tracefiles, path)
        end
    end

    if !isempty(tracefiles)
        push!(groups, GeoLifeGroup(z, tracefiles, labelfile))
    end

    GeoLife(z, groups)
end

function _parse_plt(file::String)::TraceLatLon
    positions = Point3[]
    times = DateTime[]
    reftime = geolife_reftime()

    for (i, line) in enumerate(split(file, "\r\n"))
        isempty(line) && continue  # skip empty lines
        i <= 6 && continue         # skip first `skiplines` lines

        pieces = split(line, ",") # lat, lon, <zero>, alt (or -777 if invalid), 

        lat = parse(Float, pieces[1])  # lat
        lon = parse(Float, pieces[2])  # lon
        alt = begin                    # alt
            local alt = parse(Float, pieces[4])
            alt == Float(-777) ? 0 : alt * 0.305 # feet->meters
        end
        days_since_reftime = parse(Float64, pieces[5])
        seconds_since_reftime = round(Int, days_since_reftime * 3600 * 24)
        time = reftime + Base.Dates.Second(seconds_since_reftime)

        push!(positions, latlon_deg(lat, lon, alt))
        push!(times, time)
    end

    TraceLatLon(positions, times)
end

function _parse_labels(file::String)
    labels = GeoLifeLabel[]
    for (i, line) in enumerate(split(file, "\r\n"))
        isempty(line) && continue
        i == 1 && continue # skip header line

        pieces = split(line, "\t")
        start_time = DateTime(pieces[1], "y/m/d H:M:S")
        end_time = DateTime(pieces[2], "y/m/d H:M:S")
        mode = GEOLIFE_MODE_MAP[pieces[3]]

        push!(labels, GeoLifeLabel(start_time, end_time, mode))
    end
    labels
end

Base.show(io::IO, g::GeoLife) =
    print(io, "GeoLife with ", length(g.groups), " groups and ",
          sum(map(g->length(g.tracefiles), g.groups)), " traces")

Base.show(io::IO, g::GeoLifeGroup) =
    print(io, "GeoLifeGroup with ", length(g.tracefiles),
          " traces", (isempty(g.labelfile)?"":" (labeled)"))

Base.length(g::GeoLife) = length(g.groups)
Base.length(g::GeoLifeGroup) = length(g.tracefiles)
Base.lastindex(g::GeoLife) = length(g)
Base.lastindex(g::GeoLifeGroup) = length(g)

Base.getindex(g::GeoLife, r::UnitRange) = map(i->g[i], r)
Base.getindex(g::GeoLife, i::Int) = g.groups[i]
Base.getindex(g::GeoLifeGroup, r::UnitRange) = map(i->g[i], r)
Base.getindex(g::GeoLifeGroup, i::Int) = begin
    tracestr = g.z[g.tracefiles[i]]
    _parse_plt(tracestr)
end

Base.close(g::GeoLife) = close(g.z)

Base.open(f::Function, ::Type{GeoLife}, file::String) = begin
    g = GeoLife(file)
    try     f(g)
    finally close(g)
    end
end

function get_labels(g::GeoLifeGroup)
    isempty(g.labelfile) && return [GeoLifeLabel(geolife_reftime(),
                                                 DateTime(9999),
                                                 GEOLIFE_UNKNOWN)]
    labels = _parse_labels(g.z[g.labelfile])
    sort!(labels, by=l->l.start_time)

    before_label = GeoLifeLabel(geolife_reftime(), labels[1].start_time,
                                GEOLIFE_UNKNOWN)
    gapless_labels = GeoLifeLabel[before_label]

    # preprocess -> no gaps (fill up with unknown mode)
    for label in labels
        prev_label = gapless_labels[end]
        if prev_label.end_time != label.start_time
            intermediate = GeoLifeLabel(prev_label.end_time, label.start_time,
                                        GEOLIFE_UNKNOWN)
            push!(gapless_labels, intermediate)
            push!(gapless_labels, label)
        elseif prev_label.mode == label.mode # merge
            merged = GeoLifeLabel(prev_label.start_time, label.end_time,
                                 label.mode)
            gapless_labels[end] = merged
        else
            push!(gapless_labels, label)
        end
    end

    after_label = GeoLifeLabel(gapless_labels[end].end_time, DateTime(9999),
                               GEOLIFE_UNKNOWN)
    push!(gapless_labels, after_label)
    gapless_labels
end

function split_by_mode(g::GeoLifeGroup, accepted_modes::Vector{GeoLifeMode}=GeoLifeMode[])
    labels = get_labels(g)

    traces_split = TraceLatLon[]
    modes = GeoLifeMode[]
    traceids = Int[]

    # not all modes accepted and no labels --> none match, fast fail
    if !isempty(accepted_modes) && length(labels) == 1
        return traces_split, modes, traceids
    end

    traces = g[1:end]

    sort!(traces, by=t->t.times[1])

    for (j, trace) in enumerate(traces)
        t1, t2 = 1, 0 # indices in `trace`
        k = 1         # index in `labels`

        while t2 < length(trace) && k <= length(labels)
            for i in t1:length(trace)
                if labels[k].start_time <= trace.times[i] <= labels[k].end_time
                    t2 = i
                else break end
            end

            if length(t1:t2) >= 2 # at least length 2
                if isempty(accepted_modes) || labels[k].mode ∈ accepted_modes
                    push!(traces_split, trace[t1:t2])
                    push!(modes, labels[k].mode)
                    push!(traceids, j)
                end
                t1 = t2+1
            end

            k += 1
        end
    end

    traces_split, modes, traceids
end

function geolife_export(output::IO, osmmap::OsmMap, geolife::GeoLife,
                        groups::UnitRange,
                        accepted_modes::Vector{GeoLifeMode}=GeoLifeMode[])
    map_center = osm_center(osmmap)

    for group in geolife[groups]
        local traces_latlon::Vector{TraceLatLon}
        traces, _, _ = split_by_mode(group, accepted_modes)

        if !isempty(traces)
            tracesxy = map(tr -> convert2xy(tr, map_center), traces)
            tracesxyp = map(tr -> preprocess(tr), tracesxy)
            write_cbf(output, tracesxyp)
        end

        println(group, " finished: ", length(traces), " traces written")
    end
end
