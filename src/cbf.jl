# vim: set filetype=julia

# author: Laurens Devos
#
#
#

cbf_reftime() = Dates.DateTime(1970, 1, 1, 0, 0)

function _cbf_compress_and_write(f::IO, b::IOBuffer)
    seekstart(b)
    bc = IOBuffer()
    write(bc, CodecZstd.ZstdCompressorStream(b))

    #@printf("Writing %.2fKiB compressed bytes (from %.2fKiB, %2.2f%%)\n",
    #        bc.size/1024, b.size/1024, bc.size/b.size * 100)

    seekstart(bc)
    write(f, bc) # write to file
    nothing
end

function _cbf_read_and_decompress(f::IO)::IO
    bdc::Vector{UInt8} = read(f)
    isempty(bdc) && return IOBuffer()

    bc::IOBuffer = IOBuffer(bdc)
    CodecZstd.ZstdDecompressorStream(bc)
end



# - CUSTOM BINARY FORMAT for traces: TR.CBF -----------------------------------
# Let K be the number of traces in the file. This is not stored explicitly.
#
# ```
# K × {
#   number of trace positions E (int32)
#   E × {
#     x (fl32), y (fl32), time (int32, Unix time, sec since 01/01/1970 00:00 UTC)
#   }
# }
# ```
#
# The use of float32 saves us some memory. The errors made in Beijing are about
# ±0.001 meter. Converting (lat,lon) to float32 could cause errors of up to 1m,
# which is why we don't do it (see https://en.wikipedia.org/wiki/Decimal_degrees)

""" Write a number of traces to disk in CBF format. """
function write_cbf(f::IO, traces::Vector{TraceXY})
    b = IOBuffer()
    reftime = cbf_reftime()

    for tr in traces
        E = length(tr)
        write(b, Int32(E))

        for i in 1:E
            time = div(Dates.value(tr.times[i]-reftime), 1000)
            write(b, Float32(tr.positions[i][1]))
            write(b, Float32(tr.positions[i][2]))
            write(b, Int32(time))
        end
    end

    _cbf_compress_and_write(f, b)
end

""" Read a number of traces from disk in CBF format. """
function read_cbf(::Type{Vector{TraceXY}}, f::IO)::Vector{TraceXY}
    b = _cbf_read_and_decompress(f)
    reftime = cbf_reftime()

    # avoid allocations by pre-allocating refs
    iref, fref = Ref{Int32}(), Ref{Float32}()

    traces = TraceXY[]
    while !eof(b)
        E = Int(read(b, iref)[])
        push!(traces, TraceXY(Vector{Point2}(E), Vector{DateTime}(E)))
        tr = traces[end] # shorthand

        for i in 1:E
            x = Float64(read(b, fref)[])
            y = Float64(read(b, fref)[])
            dt = Int(read(b, iref)[])

            tr.positions[i] = xy(x, y)
            tr.times[i] = reftime + Dates.Second(dt)
        end
    end

    traces
end




# - CUSTOM BINARY FORMAT for matched paths in an OSM map: PTH.CBF -------------
# Let K be the number of paths in the file. This in not stored explicitly.
# ```
# K × {
#   number of path nodes E (int32)
#   E × {
#     node (int32, node in OSM map, map not referenced explicitly)
#   }
# }
# ```

""" Write a number of paths to disk in CBF format. """
function write_cbf(f::IO, paths::Vector{Vector{Int}})
    b = IOBuffer()

    for path in paths
        E = length(path)
        write(b, Int32(E))

        for i in 1:E
            write(b, Int32(path[i]))
        end
    end

    _cbf_compress_and_write(f, b)
end

""" Read a number of paths in CBF format from disk. """
function read_cbf(::Type{Vector{Vector{Int}}}, f::IO)::Vector{Vector{Int}}
    b = _cbf_read_and_decompress(f)
    iref = Ref{Int32}()
    paths = Vector{Int}[]

    while !eof(b)
        E = Int(read(b, iref)[])

        push!(paths, Vector{Int}(E))
        p = paths[end]

        for i in 1:E
            n = Int(read(b, iref)[])
            p[i] = n
        end
    end

    paths
end




# - CUSTOM BINARY FORMAT for OSM maps: CBF ------------------------------------
#
# number of nodes N (int64)
# N × {
#   lat (fl64), lon (fl64)
# }
# number of ways M (int64)
# M × {
#   n (int64), m (int64) -> zero-indexed indices into nodes
# }

""" Write an OSM map to disk in CBF format. """
function write_cbf(f::IO, osmmap::OsmMap)
    g = osmmap.graph
    N, M = nb_nodes(g), nb_edges(g)
    b = IOBuffer()

    # write nodes
    write(b, Int64(N))
    for n in nodes(g)
        write(b, Int64(n.id)) # OSM node id
        write(b, Float64(rad2deg(n.pos[1]))) # lattitude
        write(b, Float64(rad2deg(n.pos[2]))) # longitude
    end

    # write ways
    write(b, Int64(M))
    for w in edge_indices(g)
        write(b, Int64(w[1]-1)) # zero-indexing
        write(b, Int64(w[2]-1))
    end

    _cbf_compress_and_write(f, b)
end

""" Read an OSM map in CBF format from disk. """
function read_cbf(::Type{OsmMap}, f::IO)
    g = SimpleGraph{OsmNode}()
    b = _cbf_read_and_decompress(f)

    # avoid allocations by pre-allocating refs
    iref, fref = Ref{Int64}(), Ref{Float64}()

    #gc_enable(false) # disable gc; careful w/ large maps memory, could fill up
    try              # do bulk of reading w/ gc disabled
        # read nodes
        N = Int(read(b, iref)[])
        for _ in 1:N
            id = read(b, iref)[]
            p = latlon_deg(read(b, fref)[], read(b, fref)[])
            addnode!(g, OsmNode(id, p))
        end

        # read ways
        ns = nodes(g)
        M = Int(read(b, iref)[])
        for _ in 1:M
            i = read(b, iref)[]+1 # zero-indexing
            j = read(b, iref)[]+1
            addedge!(g, ns[i], ns[j])
        end

        # find boundaries
        minlat, maxlat, minlon, maxlon = Inf, -Inf, Inf, -Inf
        for n in ns
            minlat = min(minlat, n.pos[1])
            maxlat = max(maxlat, n.pos[1])
            minlon = min(minlon, n.pos[2])
            maxlon = max(maxlon, n.pos[2])
        end

        OsmMap(g, latlon(minlat, minlon), latlon(maxlat, maxlon))
    finally
        #gc_enable(true) # make sure we turn the gc on again
    end
end
