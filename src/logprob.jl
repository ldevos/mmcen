# vim: set filetype=julia

# author: Laurens Devos
#
# see: https://en.wikipedia.org/wiki/Log_probability

primitive type LogProb 64 end

LogProb(value::Float64) = begin
    if value == zero(typeof(value))
        zero(LogProb)
    elseif value == one(typeof(value))
        one(LogProb)
    else
        reinterpret(LogProb, log(value))
    end
end

@inline _logprob2float(p::LogProb) = reinterpret(Float64, p)
@inline _float2logprob(f::Float64) = reinterpret(LogProb, f)

Base.convert(::Type{Float64}, p::LogProb) = exp(_logprob2float(p))
Base.convert(::Type{LogProb}, f::Float64) = LogProb(f)
Base.convert(::Type{LogProb}, b::Bool) = b ? one(LogProb) : zero(LogProb)

Base.zero(::Type{LogProb}) = _float2logprob(-Inf)
Base.one(::Type{LogProb}) = _float2logprob(0.0)

@inline Base.:(*)(a::LogProb, b::LogProb) = _float2logprob(_logprob2float(a) + _logprob2float(b))
@inline Base.:(+)(a::LogProb, b::LogProb) = begin
    if     _logprob2float(a) == -Inf; b
    elseif _logprob2float(b) == -Inf; a
    else   _float2logprob(_logprob2float(a) + log1p(exp(_logprob2float(b) - _logprob2float(a))))
    end
end

@inline Base.:(/)(a::LogProb, b::LogProb) = begin
    _float2logprob(_logprob2float(a) - _logprob2float(b)) # catastrophic cancellation
end

Base.isless(a::LogProb, b::LogProb) = isless(_logprob2float(a), _logprob2float(b))
Base.log(a::LogProb) = _logprob2float(a)

Base.show(io::IO, p::LogProb) = @printf(io, "%.4e*", convert(Float64, p))

