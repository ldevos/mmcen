if @isdefined(GeometryTypes)
    printstyled("GeometryTypes... ", color=:light_black)
    using GeometryTypes
end
if @isdefined(GLFW) && @isdefined(ModernGL)
    printstyled("GLFW... ", color=:light_black)
    using GLFW

    printstyled("ModernGL... ", color=:light_black)
    using ModernGL

    printstyled("Done\n", color=:green, bold=true)
end

import ModernGL.glShaderSource

const GlVertex = GeometryTypes.Point{7, GLfloat} # x,y, sin,cos, r,g,b
const GlElem = GeometryTypes.Vec{3, GLuint}      # triangle


# -----------------------------------------------------------------------------

# The vertices of our rectangle, with color
_exvertices = GlVertex[(0, 1,  10,0,  1,0,0),    # top-left
                       (0, 1, -10,0,  0,1,0),    # top-right
                       (0,-1,  10,0,  0,0,1),    # bottom-right
                       (0,-1, -10,0,  1,0,1)]    # bottom-left

_exelements = GlElem[(0,1,2), # the first triangle
                     (2,3,0)] # the second triangle



# -----------------------------------------------------------------------------

# https://github.com/JuliaGL/GLAbstraction.jl/blob/master/tutorials/drawing_polygons1.jl#L12
function _create_window(width, height)
    window_hint = [
        (GLFW.SAMPLES,      8),
        (GLFW.DEPTH_BITS,   0),

        (GLFW.ALPHA_BITS,   8),
        (GLFW.RED_BITS,     8),
        (GLFW.GREEN_BITS,   8),
        (GLFW.BLUE_BITS,    8),
        (GLFW.STENCIL_BITS, 0),
        (GLFW.AUX_BUFFERS,  0),
        (GLFW.CONTEXT_VERSION_MAJOR, 3),# minimum OpenGL v3
        (GLFW.CONTEXT_VERSION_MINOR, 0),# minimum OpenGL v3.0
        (GLFW.OPENGL_PROFILE, GLFW.OPENGL_ANY_PROFILE)
    ]

    is_apple() && push!(window_hint, (GLFW.OPENGL_FORWARD_COMPAT, GL_TRUE))

    for (key, value) in window_hint
        GLFW.WindowHint(key, value)
    end

    window = GLFW.CreateWindow(width, height, "FLOATING_WINDOW")
    GLFW.MakeContextCurrent(window)
    GLFW.SetInputMode(window, GLFW.STICKY_KEYS, GL_TRUE)

    window
end

function _create_buffer(kind, data, size)
    b = Ref(GLuint(0))
    glGenBuffers(1, b)
    glBindBuffer(kind, b[])
    glBufferData(kind, size, data, GL_STATIC_DRAW)
end

function _compile_shader(kind, src)
    shader = glCreateShader(kind)
    glShaderSource(shader, src)
    glCompileShader(shader)
    _check_shader_compilation(shader)
    shader
end

function _check_shader_compilation(shader)
    status = Ref(GLint(0))
    glGetShaderiv(shader, GL_COMPILE_STATUS, status)
    if status[] != GL_TRUE
        buffer = Vector{UInt8}(512)
        len = Ref(GLint(0))
        glGetShaderInfoLog(shader, 512, len, buffer)
        error(transcode(String, buffer[1:len[]]))
    end
end

# https://github.com/JuliaGL/GLAbstraction.jl/blob/0573aca706ab2a6dfa8f02d3bf1bcea583199beb/src/GLExtendedFunctions.jl#L11
function glShaderSource(shaderID::GLuint, shadercode::Vector{UInt8})
    shader_code_ptrs = Ptr{UInt8}[pointer(shadercode)]
    len              = Ref{GLint}(length(shadercode))
    glShaderSource(shaderID, 1, shader_code_ptrs, len)
end
glShaderSource(shaderID::GLuint, shadercode::String) = glShaderSource(shaderID, Vector{UInt8}(shadercode))

# -----------------------------------------------------------------------------

vertex_shader_src = """
#version 130
in vec2 pos;
in vec2 dir;
in vec3 color;

out vec3 vColor;

uniform vec3 transf;   // [dx, dy, zoom]
uniform vec4 params;   // [minx, miny, scale, width_scale]
uniform vec2 winsize;  // [width, height]

vec2 to_gl_coord(vec2 p, vec2 d)
{
    // Conversion from world coordinate to screen coordinate in [0,width]x[0,height]
    float sx = (p.x - params.x) * params.z + transf.x;
    float sy = (p.y - params.y) * params.z + transf.y;

    // Conversion from screen coordinate to GL coordinate in [-1,1]x[-1,1]
    float glx = (2.0 * (sx / winsize.x) - 1.0) * transf.z;
    float gly = (2.0 * (sy / winsize.y) - 1.0) * transf.z;

    // Add d scaled so that it is always equally wide, regardless of zoom level
    glx = glx + ((d.x*params.a) / (winsize.x));
    gly = gly + ((d.y*params.a) / (winsize.y));

    return vec2(glx, gly);
}

void main()
{
    vColor = color;
    vec2 p = to_gl_coord(pos, dir);

    // Output position
    gl_Position = vec4(p, 0.0, 1.0);
}
"""

fragment_shader_src = """
#version 130
in vec3 vColor;
out vec4 outColor;

void main()
{
    outColor = vec4(vColor, 1.0);
}

"""

# -----------------------------------------------------------------------------

struct GlPlot
    vertices::Vector{GlVertex}
    elements::Vector{GlElem}
end
GlPlot() = GlPlot([],[])

Base.copy(p::GlPlot) = GlPlot(copy(p.vertices), copy(p.elements))

function _plot_scale(p::GlPlot, width::Int, height::Int)
    mx, my = minimum(map(a->a[1], p.vertices)), minimum(map(a->a[2], p.vertices))
    Mx, My = maximum(map(a->a[1], p.vertices)), maximum(map(a->a[2], p.vertices))
    scale = if Mx-mx==0 && My-my==0
        println("WARNING: singularity; cannot determine scale"); 0.01
    else min(width/(Mx-mx), height/(My-my)) end
    (mx, my, .75*scale)
end

function _zoomlevel2zoomfactor(zoomlevel::Int)
    1.0 + 0.025 * zoomlevel * zoomlevel
end

mutable struct GLPlotData
    plot::GlPlot
    window::GLFW.Window
    transf_uniform::GLint
    params_uniform::GLint
    winsize_uniform::GLint
    width::Int
    height::Int

    minx::Float64
    miny::Float64
    scale::Float64

    # translation
    mouse1down::Bool
    last_mousex::Float64
    last_mousey::Float64
    translx::Float64
    transly::Float64

    # zoom
    zoomlevel::Int
end

function GLPlotData(plot::GlPlot,
                    window::GLFW.Window,
                    transf_uniform::GLint,
                    params_uniform::GLint,
                    winsize_uniform::GLint,
                    width::Int, height::Int)
    (minx, miny, scale) = _plot_scale(plot, width, height)
    GLPlotData(plot, window, transf_uniform, params_uniform,
               winsize_uniform, width, height,
               minx, miny, scale,
               false, 0.0, 0.0, 0.0, 0.0, 0)
end

_key_callback_factory(d::GLPlotData) = (window, key, scancode, action, mods) -> begin
    if key == GLFW.KEY_ESCAPE || key == GLFW.KEY_A || key == GLFW.KEY_Q
        GLFW.SetWindowShouldClose(window, true)
    end
    if key == GLFW.KEY_R && action == GLFW.PRESS
        d.translx = 0.0
        d.transly = 0.0
        d.zoomlevel = 0
        glUniform3f(d.transf_uniform, d.translx, d.transly,
                    _zoomlevel2zoomfactor(d.zoomlevel))
    end
end

_cursor_pos_callback_factory(d::GLPlotData) = (window, xpos, ypos) -> begin
    zoomf = _zoomlevel2zoomfactor(d.zoomlevel)
    xdiff = (xpos - d.last_mousex) / zoomf
    ydiff = (ypos - d.last_mousey) / zoomf
    d.last_mousex = xpos
    d.last_mousey = ypos

    if d.mouse1down
        d.translx += xdiff
        d.transly -= ydiff
        glUniform3f(d.transf_uniform, d.translx, d.transly, zoomf)
    end
end

_scroll_callback_factory(d::GLPlotData) = (window, xoffset, yoffset) -> begin
    if 0 <= d.zoomlevel+yoffset <= 1000 && !d.mouse1down
        d.zoomlevel += yoffset
        glUniform3f(d.transf_uniform, d.translx, d.transly,
                    _zoomlevel2zoomfactor(d.zoomlevel))
    end
end

_mouse_callback_factory(d::GLPlotData) = (window, button, action, mods) -> begin
    if button == GLFW.MOUSE_BUTTON_1
        action == GLFW.PRESS && !d.mouse1down && (d.mouse1down=true)
        action == GLFW.RELEASE && d.mouse1down && (d.mouse1down=false)
    elseif button == GLFW.MOUSE_BUTTON_3 && action == GLFW.PRESS
        xpos, ypos = GLFW.GetCursorPos(window)
        wx = (         xpos-d.translx) / d.scale + d.minx
        wy = (d.height-ypos-d.transly) / d.scale + d.miny
        s = @sprintf("NOTE: world coordinate:  (% 10.2f, % 10.2f)\n", wx, wy)
        s*= @sprintf("      screen coordinate: (%10d, %10d) px\n", xpos, ypos)
        printstyled(s, color=:yellow)
    end
end


# -----------------------------------------------------------------------------


function plot(p::GlPlot; width=1000, height=1000, width_scale=1.0)
    window = _create_window(width, height)
    K = length(eltype(p.vertices))

    try
        vao = Ref(GLuint(0))
        glGenVertexArrays(1, vao)
        glBindVertexArray(vao[])
        glEnable(GL_MULTISAMPLE)

        # Create the Vertex Buffer Object (VBO), Element Buffer Object (EBO)
        vbo = _create_buffer(GL_ARRAY_BUFFER, p.vertices, sizeof(p.vertices))
        ebo = _create_buffer(GL_ELEMENT_ARRAY_BUFFER, p.elements, sizeof(p.elements))

        # Compile the vertex and fragment shaders
        vertex_shader = _compile_shader(GL_VERTEX_SHADER, vertex_shader_src)
        fragment_shader = _compile_shader(GL_FRAGMENT_SHADER, fragment_shader_src)

        # Connect the shaders by combining them into a program
        shader_program = glCreateProgram()
        glAttachShader(shader_program, vertex_shader)
        glAttachShader(shader_program, fragment_shader)
        glBindFragDataLocation(shader_program, 0, "outColor") # optional

        glLinkProgram(shader_program)
        glUseProgram(shader_program)

        # Link vertex data to attributes
        pos_attrib = glGetAttribLocation(shader_program, "pos")
        glEnableVertexAttribArray(pos_attrib)
        glVertexAttribPointer(pos_attrib, 2, GL_FLOAT, GL_FALSE,
                              K*sizeof(GLfloat), C_NULL)

        dir_attrib = glGetAttribLocation(shader_program, "dir")
        glEnableVertexAttribArray(dir_attrib)
        glVertexAttribPointer(dir_attrib, 2, GL_FLOAT, GL_FALSE,
                              K*sizeof(GLfloat), Ptr{Void}(2*sizeof(GLfloat)))

        col_attrib = glGetAttribLocation(shader_program, "color")
        glEnableVertexAttribArray(col_attrib)
        glVertexAttribPointer(col_attrib, 3, GL_FLOAT, GL_FALSE,
                              K*sizeof(GLfloat), Ptr{Void}(4*sizeof(GLfloat)))

        # Location of uniforms
        transf_uniform = glGetUniformLocation(shader_program, "transf")
        params_uniform = glGetUniformLocation(shader_program, "params")
        winsize_uniform = glGetUniformLocation(shader_program, "winsize")

        # Create GLPlotData object and register event handlers
        d = GLPlotData(p, window, transf_uniform, params_uniform,
                       winsize_uniform, width, height)
        GLFW.SetKeyCallback(window, _key_callback_factory(d))
        GLFW.SetCursorPosCallback(window, _cursor_pos_callback_factory(d))
        GLFW.SetScrollCallback(window, _scroll_callback_factory(d))
        GLFW.SetMouseButtonCallback(window, _mouse_callback_factory(d))

        # Set default uniform values
        glUniform4f(params_uniform, d.minx, d.miny, d.scale, width_scale)
        glUniform3f(transf_uniform, d.translx, d.transly,
                    _zoomlevel2zoomfactor(d.zoomlevel))
        glUniform2f(winsize_uniform, convert(GLfloat, d.width),
                    convert(GLfloat, d.height))

        println("NOTE: winsize=($(d.width),$(d.height)), scale=$(round(d.scale,4)), min=($(d.minx), $(d.miny))")
        println("NOTE: press R to reset view")

        # Draw while waiting for a close event
        glClearColor(1, 1, 1, 1)
        while !GLFW.WindowShouldClose(window)
            glClear(GL_COLOR_BUFFER_BIT)
            glDrawElements(GL_TRIANGLES, 3*length(p.elements), GL_UNSIGNED_INT, C_NULL)
            GLFW.SwapBuffers(window)
            GLFW.PollEvents()
        end
    finally
        GLFW.DestroyWindow(window)
    end

    p
end

# -----------------------------------------------------------------------------

function _scatter1!(p::GlPlot, point, nbtriag::Int, w::GLfloat, color)
    K = length(p.vertices)
    p0 = GlVertex(point[1],point[2], 0,0, color[1],color[2],color[3])
    push!(p.vertices, p0)

    for (i, θ) in enumerate(linspace(0, 2π-(2π/(nbtriag+1)), nbtriag))
        push!(p.vertices, GlVertex(p0[1],p0[2], w*cos(θ),w*sin(θ), p0[5],p0[6],p0[7]))
        push!(p.elements, GlElem(K, K+i, K+(i%nbtriag)+1))
    end
end

function scatter!(p::GlPlot, points::Vector; nbtriag::Int=8, width=10.0, color=(0.0,0.0,0.0))
    w = convert(GLfloat, width)
    for x in points
        _scatter1!(p, x, nbtriag, w, color)
    end
    p
end

function _segment_pushv!(p::GlPlot, x::Point2, n::Point2, w::GLfloat, c)::Void
    push!(p.vertices, GlVertex(x[1],x[2], n[1]*w, n[2]*w, c[1],c[2],c[3]))
    push!(p.vertices, GlVertex(x[1],x[2],-n[1]*w,-n[2]*w, c[1],c[2],c[3]))
    nothing
end

function _segment_pushe!(p::GlPlot, P::Int, K::Int)::Void
    # P is the index right after the second vertex of the previous segment
    # K is the index of the first vertex of the current segment
    push!(p.elements, GlElem(P-2, P-1, K))
    push!(p.elements, GlElem(K+1, K, P-1))
    nothing
end

function _segment_start!(p::GlPlot, p0::Point2, p1::Point2, w::GLfloat, c)::Void
    n = normal(p1, p0)
    _segment_pushv!(p, p0, n, w, c)
end

function _segment_mid!(p::GlPlot, p0::Point2, p1::Point2, p2::Point2, w::GLfloat,
                       c, P::Int=length(p.vertices))::Void
    #p0 ≈ p1 && return # avoid NaN n1 normal
    K = length(p.vertices)
    max_α = 1.25 # 71.62°
    n1, n2 = normal(p1-p0), normal(p2-p1)
    α = .5(π-angle(p0, p2, p1))

    if abs(α) < max_α
        w *= convert(GLfloat, sec(α))
        n = normalize(n1+n2)
        _segment_pushv!(p, p1, n, w, c)
        _segment_pushe!(p, P, K)
    else
        _segment_pushv!(p, p1, n1, w, c) # avoid sharp angles with long points (sec large)
        _segment_pushe!(p, P, K)         # by splitting line in two (adding two additional
        _segment_pushv!(p, p1, n2, w, c) # vertices below w/ different normal)
    end
end

function _segment_end!(p::GlPlot, p1::Point2, p2::Point2, w::GLfloat, c,
                       P::Int=length(p.vertices))::Void
    K = length(p.vertices)
    n = normal(p2, p1)
    _segment_pushv!(p, p2, n, w, c)
    _segment_pushe!(p, P, K)
end

function line!(p::GlPlot, points::Vector, width=2.0, color=(0.0,0.0,0.0))
    length(points) < 2 && error("not a line")
    w = convert(GLfloat, width)
    ps = map(xy, points)

    _segment_start!(p, ps[1], ps[2], w, color)
    for (p0, p1, p2) in zip(ps[1:end-2], ps[2:end-1], ps[3:end])
        _segment_mid!(p, p0, p1, p2, w, color)
    end
    _segment_end!(p, ps[end-1], ps[end], w, color)

    p
end

function scatterline!(p::GlPlot, points::Vector, width1=5.0, width2=2.0,
                      color1=(1.0,0.0,0.0), color2=(0.0,0.0,0.0))
    line!(p, points, width2, color2)
    scatter!(p, points, nbtriag=8, width=width1, color=color1)
end

function osm!(p::GlPlot, osmmap::OsmMap; width=2.0,
              colorfn::Function=i->(0.0,0.0,0.0))
    w = convert(GLfloat, width)
    g = osmmap.graph

    # Sort gnodes such that
    gnodes, gnodeindices = let
        gnodes = nodes(g)
        #sort!(gnodes, by=n->outdegree(g,n))                                         # look at intersections
        #sort!(gnodes, alg=Base.Sort.DEFAULT_STABLE, by=n->(indegree(g,n)==0?1:0))   # after looking at ends
        sortbyval = Vector{Int}(length(gnodes))
        for (i,x) in enumerate(gnodes)
            ind, outd = indegree(g, x), outdegree(g, x)
            if ind == 0 sortbyval[i] = 99999
            else        sortbyval[i] = ind+outd end
        end
        perm = sortperm(sortbyval)
        gnodes_sorted = gnodes[perm]
        gnodeindices = Dict{OsmNode, Int}(map(p->p[2]=>p[1], enumerate(gnodes)))

        gnodes_sorted, gnodeindices
    end

    rotm = rotation_matrix(osm_center(osmmap))
    visited = Set{Tuple{OsmNode,OsmNode}}()
    stack = Vector{Tuple{Point2,Point2,OsmNode,Int}}() # (p0, p1, node, segment_start_index_K)
    outbuf = Vector{OsmNode}()

    isvisited(S, n, m) = ((n,m) ∈ S || (m,n) ∈ S)
    invalid = xy(NaN, NaN)

    # DFS through graph, starting w/ intersections
    while !isempty(gnodes)
        p0, p1, gnode, elem_offset = if isempty(stack)
            (invalid, invalid, pop!(gnodes), -1)
        else pop!(stack) end
        outgoing!(g, gnode, outbuf)

        # a new (intersection) node: push
        if elem_offset == -1
            p0 = convert2xy(gnode.pos, rotm)
            for outnode in outbuf
                isvisited(visited, gnode, outnode) && continue

                p1 = convert2xy(outnode.pos, rotm)
                _segment_start!(p, p0, p1, w, colorfn(gnodeindices[gnode]))
                elem_offset = length(p.vertices)
                push!(stack, (p0, p1, outnode, elem_offset))
                push!(visited, (gnode, outnode))
            end
        else
            unvisited_edge = false
            outnode = gnode

            for x in outbuf
                if !isvisited(visited, gnode, x)
                    outnode = x
                    unvisited_edge = true
                    break
                end
            end

            # continue a previously inserted path
            if unvisited_edge
                p2 = convert2xy(outnode.pos, rotm)
                _segment_mid!(p, p0, p1, p2, w, colorfn(gnodeindices[gnode]), elem_offset)
                elem_offset = length(p.vertices)
                push!(stack, (p1, p2, outnode, elem_offset))

                push!(visited, (gnode, outnode))
            else
                _segment_end!(p, p0, p1, w, colorfn(gnodeindices[gnode]), elem_offset)
            end
        end

        empty!(outbuf)
    end

    p
end








# -- colors -------------------------------------------------------------------

include(joinpath(@__DIR__, "colors.jl"))

colorfn_centrality(M::MMMatrix) = begin
    intensities = centralities(M)
    colorfn(intensities)
end

colorfn_centrality_diff(A::MMMatrix, B::MMMatrix) = begin
    intensitiesA = centralities(A)
    intensitiesB = centralities(B)
    intensitiesA -= intensitiesB
    map!(abs, intensitiesA, intensitiesA)
    colorfn(intensitiesA)
end

colorfn_rowmax(M::MMMatrix) = begin
    intensities = rowmax(M)
    colorfn(intensities)
end

colorfn_boundaries(intensities::Vector{Float64},
                   colors,
                   intensity_offset::Float64=-Inf) = begin
    intensities_sorted = sort(intensities)
    N, K = length(intensities), length(colors)
    k = max(1, searchsortedlast(intensities_sorted, intensity_offset))
    range = map(x->round(Int, x), linspace(k, N, K))
    boundaries = intensities_sorted[range]
    println("color intensities: ", boundaries[1], " -> ", boundaries[end])
    boundaries
end

colorfn(intensities::Vector{Float64};
        colors=PerceptualColourMapsHEAT(),
        intensity_offset::Float=-Inf,
        boundaries::Vector{Float}=colorfn_boundaries(intensities, colors, intensity_offset)) = begin
    return i -> begin
        k = searchsortedlast(boundaries, intensities[i])
        k == 0 ? (.5, .5, .5) : colors[k]
    end
end






# -- line specialization ------------------------------------------------------

function mmtrace!(p::GlPlot, tr::TraceXY; width=4.0, color=(0.8,0.2,0.1),
                  marks=true, glow::Float64=0.0)
    if glow>0.0
        mmtrace!(p, tr; width=glow*width, color=(1,1,1), marks=marks, glow=0.0)
    end

    if marks
        scatterline!(p, tr.positions, 2*width, .5*width, .9.*color, color)
    else
        line!(p, tr.positions, width, color)
    end
end

function mmpath!(p::GlPlot, mm::MapMatcher, path::Vector{Int}; width=4.0,
                 color=(0.2,0.8,0.1))
    isempty(path) && return p
    points = Vector{Point2}()
    push!(points, mm.xy[mm.hmm_states[path[1]][1]])
    for i in path
        push!(points, mm.xy[mm.hmm_states[i][2]])
    end
    line!(p, points, width, color)
end

function mmtracepath!(p::GlPlot, mm::MapMatcher, tr::TraceXY,
                      path::Vector{Int}; width=4.0, tracec=(0.8,0.2,0.1),
                      pathc=(0.2,0.8,0.1))
    mmpath!(p, mm, path; width=width, color=pathc)
    mmtrace!(p, tr; width=width, color=tracec)
end


# -- batch --------------------------------------------------------------------

function mmpaths(p::GlPlot, mm::MapMatcher, paths::Vector{Vector{Int}}; kwargs...)
    colors = diffcolors()
    K = length(colors)
    pc = copy(p)
    ProgressMeter.@showprogress for (i,path) in enumerate(paths)
        mmpath!(pc, mm, path; color=colors[i%K+1], kwargs...)
    end
    pc
end




# -- non-modifying function versions ------------------------------------------


for (fsymb, fcopysymb) in 
    [(:scatter!,     :scatter),
     (:line!,        :line),
     (:scatterline!, :scatterline),
     (:mmtrace!,     :mmtrace),
     (:mmpath!,      :mmpath),
     (:mmtracepath!, :mmtracepath)]

    @eval ($fcopysymb)(args...; kwargs...) = begin
        pcopy = copy(args[1])
        ($fsymb)(pcopy, args[2:end]...; kwargs...)
        pcopy
    end
end

function osmplot(osmmap::OsmMap; kwargs...)
    p = GlPlot()
    osm!(p, osmmap; kwargs...)
    p
end




# -----------------------------------------------------------------------------

function Base.show(io::IO, p::GlPlot)
    print(io, "GlPlot(")
    if !isempty(p.vertices)
        print(io,
            "$(minimum(map(a->a[1], p.vertices)))->",
            "$(maximum(map(a->a[1], p.vertices))), ",
            "$(minimum(map(a->a[2], p.vertices)))->",
            "$(maximum(map(a->a[2], p.vertices))), ")
    end
    print(io, "#V=$(length(p.vertices)), ",
            "#E=$(length(p.elements)))")

end
