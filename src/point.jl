# vim: set filetype=julia

# author: Laurens Devos

const Point2 = GeometryTypes.Point2{Float}
const Point3 = GeometryTypes.Point3{Float}
const Coord2 = GeometryTypes.Vec2{Int}

latlon(lat, lon, alt=.0) = Point3(lat, lon, alt)
latlon_deg(lat, lon, alt=.0) = Point3(deg2rad(lat), deg2rad(lon), alt)

xy(a) = Point2(a[1], a[2])
xy(x, y) = Point2(x, y)
xyz(x, y, z) = Point3(x, y, z)
coord(i, j) = Coord2(i, j)

struct UnitPlanet end
struct Earth end
radius(::Type{UnitPlanet}) = 1.0e0
radius(::Type{Earth})      = 6371.0e3

# Ref: https://en.wikipedia.org/wiki/Reference_ellipsoid
# Coordinate system: latlon( 0°,90°) -> (1,0,0)
#                    latlon(90°, 0°) -> (0,1,0)
#                    latlon( 0°, 0°) -> (0,0,1)
function latlon2xyz(p::Point3, ::T=Earth()) where T
    equaR = polarR = radius(T)

    coslat, sinlat = cos(p[1]), sin(p[1])
    coslon, sinlon = cos(p[2]), sin(p[2])
    nlat = (equaR^2) / sqrt((equaR*coslat)^2 + (polarR*sinlat)^2)

    y = (nlat + p[3]) * coslat * sinlon
    z = (((polarR/equaR)^2) * nlat + p[3]) * sinlat
    x = (nlat + p[3]) * coslat * coslon

    xyz(y, z, x)
end

# in-place latlon2xyz conversion of a vector of positions given in (lat,lon)
function latlon2xyz!(ps::Vector{Point3}, planet::T=Earth()) where T
    map!(ps, ps) do p
        latlon2xyz(p, planet)
    end
    nothing
end

distance_sq(p::Point2, q::Point2) = (q[1]-p[1])^2 + (q[2]-p[2])^2
distance_sq(p::Point3, q::Point3) = (q[1]-p[1])^2 + (q[2]-p[2])^2 + (q[3]-p[3])^2
distance(p::Point2, q::Point2) = sqrt(distance_sq(p, q))

# Dot product of vectors (p-c) and (q-c)
dot(p::Point2, q::Point2, c::Point2) = dot(p-c, q-c)
dot(p::Point2, q::Point2) = p[1]*q[1] + p[2]*q[2]

# Angle (0 <= α <= 2π) between vectors (p-c) and (q-c) (right-hand)
function Base.angle(p::Point2, q::Point2, c::Point2)
    pc, qc = p-c, q-c
    n = dot(pc, qc)
    d = norm(pc)*norm(qc)
    r = n / d

    α = if r >= 1.0     0.0
    elseif r <= -1.0    Float64(π)
    else                acos(r)     end

    cross(pc, qc) < 0 && (α = 2π-α) # angle > 180°

    α
end

# Compute the normalized normal of vector `vec`.
function normal(vec::Point2)
    normalize(xy(vec[2], -vec[1])) # right turn
end
normal(p::Point2, q::Point2) = normal(p-q)

# Distance from o to line (p, q)
function distance2line_sq(p::Point2, q::Point2, o::Point2)
    qp = q-p
    (((qp[1])*(p[2]-o[2]) - (p[1]-o[1])*(qp[2]))^2) / ((qp[1])^2 + (qp[2])^2)
end

# Distance from o to segment (p, q)
function distance2seg_sq(p::Point2, q::Point2, o::Point2)
    dot1 = dot(q, o, p)
    dot2 = dot(p, o, q)
    if dot1 < 0 # angle greater than 90° centered at p
        distance_sq(p, o)
    elseif dot2 < 0 # angle grater than 90° center at q
        distance_sq(q, o)
    else
        distance2line_sq(p, q, o)
    end
end

rotx(θ) = StaticArrays.SArray{Tuple{3,3},Float,2,9}(1,0,0, 0,cos(θ),-sin(θ), 0,sin(θ),cos(θ))
roty(θ) = StaticArrays.SArray{Tuple{3,3},Float,2,9}(cos(θ),0,sin(θ), 0,1,0, -sin(θ),0,cos(θ))
rotz(θ) = StaticArrays.SArray{Tuple{3,3},Float,2,9}(cos(θ),-sin(θ),0, sin(θ),cos(θ),0, 0,0,1)

function rotation_matrix(rot)
    rotm = (rotx(-rot[1]) * roty(rot[2]))
end

function rotate2xyplane(p::Point3, rotation_matrix::StaticArrays.SArray)
    tmp = StaticArrays.@SVector [ p[1], p[2], p[3] ]
    rotation_matrix * tmp
end

function rotate2xyplane!(pv::Vector{Point3}, rot) # in-place rotation
    rotm = rotation_matrix(rot)
    for i in 1:length(pv)
        pv[i] = rotate2xyplane(pv[i], rotm)
    end
    nothing
end

function convert2xy(pv::Vector{Point3}, rot) # pv in lat,lon
    pvv = copy(pv)
    latlon2xyz!(pvv)
    rotate2xyplane!(pvv, rot)
    map(p -> xy(p[1], p[2]), pvv)
end

function convert2xy(p::Point3, rotation_matrix::StaticArrays.SArray)
    tmp = rotate2xyplane(latlon2xyz(p), rotation_matrix)
    xy(tmp[1], tmp[2])
end
