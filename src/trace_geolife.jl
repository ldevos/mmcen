# vim: set filetype=julia

# author: Laurens Devos
#
# Reader for Microsoft GeoLife dataset

if @isdefined(ZipFile)
    printstyled("ZipFile... ", color=:light_black)
    import ZipFile

    printstyled("Done\n", bold=true, color=:green)
end

@enum(GeoLifeMode,
      GEOLIFE_WALK,
      GEOLIFE_BIKE,
      GEOLIFE_BUS,
      GEOLIFE_CAR,
      GEOLIFE_TAXI,
      GEOLIFE_SUBWAY,
      GEOLIFE_TRAIN,
      GEOLIFE_AIRPLANE,
      GEOLIFE_BOAT,
      GEOLIFE_RUN,
      GEOLIFE_MOTORCYCLE)

GEOLIFE_MODE_MAP = Dict{String, GeoLifeMode}(
    "walk"       => GEOLIFE_WALK,
    "bike"       => GEOLIFE_BIKE,
    "bus"        => GEOLIFE_BUS,
    "car"        => GEOLIFE_CAR,
    "taxi"       => GEOLIFE_TAXI,
    "subway"     => GEOLIFE_SUBWAY,
    "train"      => GEOLIFE_TRAIN,
    "airplane"   => GEOLIFE_AIRPLANE,
    "boat"       => GEOLIFE_BOAT,
    "run"        => GEOLIFE_RUN,
    "motorcycle" => GEOLIFE_MOTORCYCLE)

struct GeoLifeLabel
    start_time::DateTime
    end_time::DateTime
    mode::GeoLifeMode
end

# TODO cache result: now files can only be read once
struct GeoLifeGroup
    group_id::Int
    files::Vector{ZipFile.ReadableFile}
    labelsf::Union{Void, ZipFile.ReadableFile}
end
GeoLifeGroup(group_id, labelsf) = GeoLifeGroup(group_id, ZipFile.ReadableFile[], labelsf)

struct GeoLife
    dir::ZipFile.Reader
    groups::Vector{GeoLifeGroup}

    function GeoLife(zipfile::String)  # use open(GeoLife) do ... end syntax
        dir = ZipFile.Reader(zipfile)
        groups = Vector{GeoLifeGroup}()
        labelsf::Union{Void, ZipFile.ReadableFile} = nothing

        for f in dir.files
            slt = split(f.name, "/")

            if length(slt) == 5 && slt[5] == ""
                group_id = parse(Int, slt[3])
                push!(groups, GeoLifeGroup(group_id, labelsf))
                labelsf = nothing
                @assert length(groups) == group_id + 1
            elseif last(slt) == "labels.txt"
                labelsf = f
            elseif endswith(last(slt), ".plt")
                push!(last(groups).files, f)
            end
        end

        new(dir, groups)
    end
end

function Base.getindex(geolife::GeoLife, i::Int)::GeoLifeGroup
    geolife.groups[i]
end

function Base.getindex(group::GeoLifeGroup, i::Int)::TraceLatLon
    file = group.files[i]
    parse_plt_stream(file; skiplines=6)
end

function haslabels(group::GeoLifeGroup)::Bool
    group.labelsf != nothing
end

function labels(group::GeoLifeGroup)::Vector{GeoLifeLabel}
    if haslabels(group)
        parse_labels(group.labelsf)
    else
        GeoLifeLabel[]
    end
end

function Base.open(f::Function, ::Type{GeoLife}, zipfile::String)
    gl = GeoLife(zipfile)
    try
        f(gl)
    finally
        close(gl)
    end
end

function Base.close(gl::GeoLife)
    close(gl.dir)
    empty!(gl.groups)
    nothing
end

function Base.show(io::IO, geolife::GeoLife)
    groups = length(geolife.groups)
    files = sum(map(g -> length(g.files), geolife.groups))
    @printf(io, "GeoLife trace set with %d groups and %d trace files in total", groups, files)
end

function Base.show(io::IO, group::GeoLifeGroup)
    @printf(io, "GeoLife group containing %d trace files", length(group.files))
end

geolife_reftime() = DateTime(1899, 12, 30)

function parse_plt_stream(stream::IO; skiplines=0)::TraceLatLon
    positions = Point3[]
    times = DateTime[]
    reftime = geolife_reftime()

    for (i, line) in enumerate(eachline(stream))
        i <= skiplines && continue # skip first `skiplines` lines

        pieces = split(line, ",") # lat, lon, <zero>, alt (or -777 if invalid), 

        lat = parse(Float, pieces[1])  # lat
        lon = parse(Float, pieces[2])  # lon
        alt = begin                    # alt
            local alt = parse(Float, pieces[4])
            alt == Float(-777) ? 0 : alt * 0.305 # feet->meters
        end
        days_since_reftime = parse(Float64, pieces[5])
        seconds_since_reftime = round(Int, days_since_reftime * 3600 * 24)
        time = reftime + Base.Dates.Second(seconds_since_reftime)

        push!(positions, latlon_deg(lat, lon, alt))
        push!(times, time)
    end

    TraceLatLon(positions, times)
end

function parse_labels(stream::IO)
    labels = GeoLifeLabel[]
    for (i, line) in enumerate(eachline(stream))
        i == 1 && continue # skip header line

        pieces = split(line, "\t")
        start_time = DateTime(pieces[1], "y/m/d H:M:S")
        end_time = DateTime(pieces[2], "y/m/d H:M:S")
        mode = GEOLIFE_MODE_MAP[pieces[3]]

        push!(labels, GeoLifeLabel(start_time, end_time, mode))
    end
    labels
end

function as_vector(group::GeoLifeGroup)::Vector{TraceLatLon}
    N = length(group.files)
    v = Vector{TraceLatLon}(N)
    meter = ProgressMeter.Progress(N; dt=0.1,
                                   desc="Reading traces ",
                                   color=:default,
                                   barglyphs=ProgressMeter.BarGlyphs("[=> ]"))
    for i in 1:N
        trace = group[i]
        v[i] = trace
        ProgressMeter.next!(meter)
    end
    v

    # TODO EXTEND THIS S.T. FILTER BY MODES!!
end

function group_as_vector(file::String, group_number::Int)::Vector{TraceLatLon}
    open(GeoLife, file) do gl
        as_vector(gl[group_number])
    end
end

function group_as_vector(file::String, group_number::Int, modes::Vector{GeoLifeMode})::Vector{TraceLatLon}
    open(GeoLife, file) do gl
        as_vector(gl[group_number], modes)
    end
end

function export_geolife_cbf(file::String, export_folder::String, osmmap::OsmMap,
                            range)
    open(GeoLife, file) do gl
        K = length(gl.groups)
        map_center = osm_center(osmmap)

        first(range) < 1 && (range = 1:last(range); println("Corrected range: $range"))
        last(range) > K && (range = first(range):K; println("Corrected range: $range"))

        println("Exporting $(length(range)) out of $K trace groups to '$export_folder'")

        for k in range
            tic()

            group = gl[k]
            N = length(group.files)
            filename = joinpath(export_folder, "geolife$k.tr.cbf")

            println("Exporting group $k: $group to '$filename'")
            traces = as_vector(group)

            println("Converting lat/lon trace to XY coordinates...")
            tracesxy = map(tr -> convert2xy(tr, map_center), traces)

            println("Preprocessing traces...")
            tracesxyr = map(tr -> preprocess(tr), tracesxy)

            println("Preprocessing traces...")
            open(filename, "w") do f write_cbf(f, tracesxyr) end

            println("Done with group $k in $(round(toq(), 2)) seconds")
            println()
        end
    end
end
