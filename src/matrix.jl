# vim: set filetype=julia

# author: Laurens Devos


ufset(N::Int) = collect(1:N) # union find set

function uffind(set::Vector{Int}, i::Int)
    while set[i] != i
        prev = i
        i = set[i]
        set[prev] = set[i] # compression
    end
    i
end

function ufpoint!(set::Vector{Int}, i::Int, j::Int) # make i point to ufind(j)
    iroot, jroot = uffind(set, i), uffind(set, j)
    set[iroot] = jroot
end


# -----------------------------------------------------------------------------

function rownormalize(M::SparseArrays.SparseMatrixCSC)
    spdiagm(inv.(sum(M, 2)[:])) * M
end

function centralities(D::Vector{Float64}, V::Matrix{Float64})
    N = size(V, 1)
    C = Vector{Float64}(N)

    Dexp = exp.(D)

    for i in 1:N
        C[i] = sum(Dexp .* V[i,:].^2)
    end

    C
end

# -----------------------------------------------------------------------------


@enum MatrixNodeKinds Regular Redundant Absorbing Unvisited

struct MMMatrix
    M::SparseArrays.SparseMatrixCSC
    rowmap::Vector{Int}
    nodekinds::Vector{MatrixNodeKinds}
end

function MMMatrix(M::MMMatrix, Mreplace::SparseArrays.SparseMatrixCSC)
    MMMatrix(Mreplace, M.rowmap, M.nodekinds)
end

function centralities(M::MMMatrix)
    N = length(M.rowmap)
    d = diag(M.M)
    collect(map(i->i==-1 ? 0.0 : d[i], M.rowmap))
end

function rowmax(M::MMMatrix)
    N = length(M.rowmap)
    d = maximum(M.M, 2)
    collect(map(i->i==-1 ? 0.0 : d[i], M.rowmap))
end

""" Count the number of times a transion (node1->node2) is made in the paths. """
function _count_frequencies(mm::MapMatcher, paths::Vector{Vector{Int}})
    freqs = Dict{Tuple{Int,Int},Int}()
    for path in paths
        for i in path
            node1, node2 = mm.hmm_states[i]
            node1 == node2 && error("path contains consecutive duplicate -> self loop")
            freqs[(node1, node2)] = get(freqs, (node1, node2), 0) + 1
        end
    end
    freqs
end

""" Calculate the number of incoming and outgoing connections for each node. """
function _empirical_degrees(freqs::Dict{Tuple{Int,Int}, Int}, nb_nodes::Int)
    incount, outcount = zeros(nb_nodes), zeros(nb_nodes)
    for (node1, node2) in keys(freqs)
        incount[node2]  += 1
        outcount[node1] += 1
    end
    incount, outcount
end

""" Generate the map matching matrix. """
function mmmatrix(mm::MapMatcher, paths::Vector{Vector{Int}}, normalize::Bool=true)
    nb_states = length(mm.hmm_states)
    nb_nodes = length(mm.xy)

    freqs = _count_frequencies(mm, paths)
    indegree, outdegree = _empirical_degrees(freqs, nb_nodes) # emprical in-out degree

    # classify nodes --> resulting lists are sorted
    nodekinds = Vector{MatrixNodeKinds}(nb_nodes)
    unvisited = Int[]
    absorbing = Int[]
    redundant = Int[]
    regular = Int[]
    for node in 1:nb_nodes
        ic, oc = indegree[node], outdegree[node]
        ic==0 && oc==0 && (push!(unvisited, node); nodekinds[node]=Unvisited; continue)
        ic> 0 && oc==0 && (push!(absorbing, node); nodekinds[node]=Absorbing; continue)
        ic==1 && oc==1 && (push!(redundant, node); nodekinds[node]=Redundant; continue)
                           push!(regular,   node); nodekinds[node]=Regular
    end

    # traffic through redundant i equals [redin[i],redout[i]] in matrix
    redin, redout = ufset(nb_nodes), ufset(nb_nodes)
    for (node1, node2) in keys(freqs)
        nodekinds[node1]==Redundant && ufpoint!(redout, node1, node2)
        nodekinds[node2]==Redundant && ufpoint!(redin, node2, node1)
    end

    # update frequencies of redundant nodes k
    for r in redundant
        rin, rout = uffind(redin, r), uffind(redout, r)
        rin == rout && continue # avoid self loop

        # update f_ij = f_ik --> there is only one k --> test (rin,r) ∈ freqs
        haskey(freqs, (rin, r)) && (freqs[(rin, rout)] = freqs[(rin, r)])
    end
    for (node1, node2) in keys(freqs)
        if nodekinds[node1]==Redundant || nodekinds[node2]==Redundant
            delete!(freqs, (node1, node2))
        end
    end

    # update frequencies of absorbing
    for a in absorbing
        freqs[(a,a)] = 1
    end

    # construct row lookup table: first come all regular, then absorbing
    rowlookup = let
        d, k = Dict{Int, Int}(), 1
        for r in regular;   d[r] = k; k += 1 end
        for a in absorbing; d[a] = k; k += 1 end
        d
    end

    # construct matrix
    Is, Js, Fs = Int[], Int[], Float64[]
    for ((node1, node2), freq) in freqs
        haskey(rowlookup, node1) || error("[$node1]->$node2 = $freq; rowlookup fail")
        haskey(rowlookup, node2) || error("$node1->[$node2] = $freq; rowlookup fail")
        node1 == node2 && nodekinds[node1]!=Absorbing && error("diagonal $node1 = $freq and not absorbing")

        push!(Is, rowlookup[node1])
        push!(Js, rowlookup[node2])
        push!(Fs, convert(Float64, freq))
    end

    Nm = length(regular) + length(absorbing); @assert length(rowlookup) == Nm
    freqM = sparse(Is, Js, Fs, Nm, Nm)

    # make M row-stochastic
    M = normalize ? rownormalize(freqM) : freqM

    # construct rowmap & nodekinds: redundant point to the row it was reduced to
    rowmap = zeros(Int, nb_nodes)
    for node in 1:nb_nodes
        kind = nodekinds[node]

       # println("$node [$kind]")

        rownumber = -1
        if kind == Regular || kind == Absorbing
            rownumber = rowlookup[node]
        elseif kind == Redundant
            rin = uffind(redin, node)
            rownumber = nodekinds[rin]!=Redundant ? rowlookup[rin] : -1
        end

        rowmap[node] = rownumber
    end

    MMMatrix(M, rowmap, nodekinds)
end





Base.show(io::IO, m::MMMatrix) = (k=size(m.M,1); print(io, "MMMatrix($k×$k)"))







# -----------------------------------------------------------------------------

# f! function (TERM, k) -> TERM_next, modify TERM
function _naive_matrix_fn(A, ACC, TERM, f!, maxiter, tol, trunc_below, maxmemsz)
    # SCALING DOES NOT CHANGE norm(TERM,1) / norm(ACC,1) -> number of iterations unaffected by scaling
    ## scale A: (e^(A/a)^a) = e^A (see Julia implementation of `expm`)
    #s = log2(norm(A, 1) / 5.4) # power of 2 later reversed by squaring
    #si = ceil(Int, s)
    #s > 0 && (A /= 2^si; trunc_below /= 2^si)

    for k in 1:maxiter
        TERM = f!(TERM, k)
        ACC += TERM

        # Truncate all values below `trunc_below`
        map!(x -> x<trunc_below ? 0.0 : x, TERM.nzval, TERM.nzval)
        dropzeros!(TERM)
        map!(x -> x<trunc_below ? 0.0 : x, ACC.nzval, ACC.nzval)
        dropzeros!(ACC)

        # Compute relative size of TERM and memory usage
        ϵ = norm(TERM,1) / norm(ACC, 1)
        memsz = sizeof(ACC.nzval) + sizeof(TERM.nzval)

        if memsz > 1024^2
            Printf.@printf("NOTE iter: %2i, memory: %6.2fMiB, ϵ: %.2e\n", k, memsz/(1024^2), ϵ)
        end

        ϵ < tol && break
        memsz > maxmemsz && break
    end

    ## undo scaling of A
    #s > 0 && (for t in 1:si; ACC *= ACC end)

    ACC
end

"""
Compute the matrix exponential by explicitly computing the sum of the weighed
matrix powers.

I've tried adapting the 'scaling and squaring method', but solving the linear
system turned out to be more demanding than this.

    Higham, Nicholas J. "The scaling and squaring method for the matrix exponential
    revisited." SIAM Journal on Matrix Analysis and Applications 26.4 (2005):
    1179-1193.

    Tried adapting this implementation:
    See: https://github.com/JuliaLang/julia/blob/v0.6.2/base/linalg/dense.jl#L429
"""
function naive_exponential(A::SparseArrays.SparseMatrixCSC;
                           maxiter=typemax(Int),
                           tol=eps(Float64),
                           trunc_below=eps(Float64),
                           maxmemsz=500*1024^2) # 500MiB max memory usage
    ACC = speye(A)
    TERM = speye(A)

    f! = (TERM, k) -> begin
        TERM *= A
        TERM /= convert(Float64, k)
    end

    _naive_matrix_fn(A, ACC, TERM, f!, maxiter, tol, trunc_below, maxmemsz)
end

"""
Estrada, Ernesto, and Desmond J. Higham. "Network properties revealed through
matrix functions." SIAM review 52.4 (2010): 696-714.

Weight the matrix powers by an approximation of the number of paths in the
fully connected network K_N.
"""
function naive_resolvent(A::SparseArrays.SparseMatrixCSC,
                         maxiter=typemax(Int),
                         tol=eps(Float64),
                         trunc_below=0.01*eps(Float64),
                         maxmemsz=500*1024^2) # 500MiB max memory usage
    TERM = speye(A)
    ACC = copy(A)
    N = maximum(size(A))
    N1 = convert(Float64, N-1)

    f! = (TERM, _) -> begin
        TERM *= A
        TERM /= N1
    end

    _naive_matrix_fn(A, ACC, TERM, f!, maxiter, tol, trunc_below, maxmemsz)
end



# Shorthand for 'best possible way', in our case, naive
function Base.exp(M::MMMatrix)
    Mexp = naive_exponential(M.M)
    MMMatrix(M, Mexp)
end

# -----------------------------------------------------------------------------
