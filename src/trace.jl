# vim: set filetype=julia

# author: Laurens Devos

struct Trace{PosType}
    positions::Vector{PosType}
    times::Vector{Dates.DateTime}

    function Trace{PT}(ps::Vector{PT}, ts::Vector{Dates.DateTime}) where {PT}
        @assert length(ps) == length(ts) "#positions != #times"
        new{PT}(ps, ts)
    end
end

const TraceLatLon = Trace{Point3} # lat, lon, alt in rad,rad,meter
const TraceXY = Trace{Point2}     # x, y in meter,meter

function convert2xy(trace::TraceLatLon, rot)
    positionsxy = convert2xy(trace.positions, rot)
    TraceXY(positionsxy, trace.times)
end

function remove_trues(trace::TraceXY, bitv::BitVector)
    N = length(trace)
    count = 0
    new_positions = Vector{Point2}()
    new_times = Vector{DateTime}()
    for i in 1:N
        if !bitv[i]
            push!(new_positions, trace.positions[i])
            push!(new_times, trace.times[i])
        else
            count += 1
        end
    end
    println("NOTE: removed $count out of $N observations")
    TraceXY(new_positions, new_times)
end

Base.length(trace::Trace) = length(trace.positions)
Base.lastindex(trace::Trace) = lastindex(trace.positions)
Base.copy(t::TraceXY) = TraceXY(copy(t.positions), copy(t.times))
Base.show(io::IO, trace::TraceLatLon) = begin
    print(io, "Trace with ", length(trace), " positions [",
          minimum(trace.times), "->", maximum(trace.times), "]")
end
Base.show(io::IO, trace::TraceXY) = begin
    meandist = round(mean(norm.(diff(trace.positions))), 2)
    print(io, "Trace with ", length(trace), " positions $(meandist)m apart on average [",
          minimum(trace.times), "->", maximum(trace.times), "]")
end
Base.getindex(t::Trace, u::UnitRange) = typeof(t)(t.positions[u], t.times[u])

# -----------------------------------------------------------------------------

"""
    preprocess(trace)

This function performs two tasks:
    1) Remove outliers by looking at velocities and distances.
    2) Remove superfluous observations by looking at distances between
       consecutive observations.
"""
function preprocess(trace::TraceXY, max_distance::Float=50.0)
    tr = copy(trace) # don't modify given trace

    distance_jump_reduction(tr)
    speed_jump_reduction(tr)

    thin_trace(tr, max_distance)
end

function distances_sq(trace::TraceXY)
    ds = Vector{Float}(length(trace)); ds[1] = 0
    for i in 2:length(trace)
        ds[i] = distance_sq(trace.positions[i-1], trace.positions[i])
    end
    ds
end

function distances(trace::TraceXY)
    sqrt.(distances_sq(trace))
end

function speeds(trace::TraceXY)
    vs = Vector{Float}(length(trace)); vs[1] = 0
    for i in 2:length(trace)
        d = distance(trace.positions[i-1], trace.positions[i])     # meter
        t = Dates.value(trace.times[i] - trace.times[i-1])/1000    # seconds
        vs[i] = d / t                                              # meter/second
    end
    vs
end

function angles(trace::TraceXY)
    as = Vector{Float}(length(trace)); as[1] = 0; as[end] = 0
    ps = trace.positions
    for i in 2:length(trace)-1
        as[i] = angle(ps[i-1], ps[i+1], ps[i])
    end
    as
end

function distance_outliers(trace::Trace)
    ds = distances(trace) # meter
    pd = Distributions.fit(Distributions.Exponential, ds)

    errs = Distributions.ccdf.(pd, ds) .< 0.001
    errs
end

function speed_outliers(trace::Trace)
    vs = speeds(trace) # meter/second

    # CFAR algorithm: https://en.wikipedia.org/wiki/Constant_false_alarm_rate
    # solved using a convolution
    window_sz, guard_sz = 100, 10
    g = [ones(window_sz); zeros(2*guard_sz); ones(window_sz)] / (2.0*window_sz);
    c = conv(vs, g)[window_sz+guard_sz:end-window_sz-guard_sz]
    lvls = vs ./ c

    outliers = lvls .> 5.0
    vs, outliers
end

function distance_jump_reduction(trace::Trace, lookahead::Int=20)
    N = length(trace)
    ps = trace.positions
    dm = 50.0 # max distance trigger
    i = 1
    count = 0

    while i < N
        i += 1

        di = distance(ps[i-1], ps[i])
        dm = 0.95*dm + 0.05*2.0*di # moving 2*average

        di <= dm && continue     # distance is fine, nothing to change, move on

        jbest, djbest = i, di
        for j in i+1:min(i+lookahead, N)
            dj = distance(ps[i-1], ps[j])
            if dj < djbest       # smaller is better
                jbest, djbest = j, dj
            end
        end

        jbest == i && continue   # no better j found

        for k in i:jbest-1           # linearly interpolatie observations...
            t = (k-i) / (jbest-i)    # ...between i-1 and jbest
            ps[k] = t*ps[i-1] + (1-t)*ps[jbest]
            count += 1
            #@printf("ps[%d] = %s (t=%.2f) because di=%.2f (i=%d) > dj=%.2f (j=%d)\n", k, ps[k], t, di, i, djbest, jbest)
        end

        i = i-1                  # check again after changes
    end

    #println("NOTE: distance_jump_reduction changed $count points")
end

function angle_jump_reduction(trace::Trace, lookahead::Int=5) # doesn't work well
    N = length(trace)
    ps = trace.positions
    i = 1
    ϵ = deg2rad(5)

    while i < N-1
        i += 1

        ai = angle(ps[i-1], ps[i+1], ps[i])

        ai <= 2.5 && continue

        jbest, ajbest = i, ai
        for j in i+1:min(i+lookahead, N)
            aj = angle(ps[i-1], ps[j], ps[i])
            if aj > ajbest + ϵ  # closer to 180° is better
                jbest, ajbest = j, aj
            end
        end

        jbest == i && continue  # no better j found

        for k in i+1:jbest-1         # linearly interpolatie observations...
            t = (k-i) / (jbest-i)    # ...between i-1 and jbest
            ps[k] = t*ps[i] + (1-t)*ps[jbest]
            #@printf("ps[%d] = %s (t=%.2f) because ai=%.2f (i=%d) > aj=%.2f (j=%d)\n", k, ps[k], t, rad2deg(ai), i, rad2deg(ajbest), jbest)
        end

        i = i - 1
    end
end

function speed_jump_reduction(trace::Trace, lookahead::Int=10)
    N = length(trace)
    ps = trace.positions
    ts = trace.times
    vs, outliers = speed_outliers(trace)
    i = 2
    count = 0

    # compute avg speed from pos i to j
    dfn = (i, j) -> begin
        p, q = ps[i], ps[j]
        dt = Dates.value(ts[j] - ts[i]) / 1000.0 # sec
        distance(p, q) / dt
    end

    while i < N
        i += 1
        outliers[i] || continue

        jbest, vjbest = i, vs[i]
        for j in i+1:min(i+lookahead, N)
            vj = dfn(i-1, j)
            if vj < vjbest         # smaller is better
                jbest, vjbest = j, vj
            end
        end

        jbest == i && continue     # no better j found

        for k in i:jbest-1         # linear interpolation
            t = (k-i) / (jbest-i)  # ...between i-1 and jbest
            ps[k] = t*ps[i-1] + (1-t)*ps[jbest]
            count += 1
            #@printf("ps[%d] = %s (t=%.2f) because di=%.2f (i=%d) > dj=%.2f (j=%d)\n", k, ps[k], t, vs[i], i, vjbest, jbest)
        end

        i = jbest
    end

    #println("NOTE: speed_jump_reduction changed $count points")
end

function thin_trace(trace::Trace, max_distance::Float)
    N = length(trace); N<=2 && return trace
    ps = trace.positions
    ts = trace.times
    mdsq = max_distance^2

    thinps = Vector{Point2}();   push!(thinps, ps[1])
    thints = Vector{DateTime}(); push!(thints, ts[1])

    i = 2               # first untested point in original trace
    n = 0               # number of cumulated points

    cumpos, cumdur = xy(.0,.0), Dates.Millisecond(0)

    while i < N - 1     # -1 because we'll use the last point as is
        if distance_sq((cumpos+ps[i]) ./ (n+1), thinps[end]) < mdsq
            cumpos += ps[i]
            cumdur += ts[i]-thints[end]
            i, n = i+1, n+1
        elseif n == 0   # a single point is farther away than `max_distance`
            push!(thinps, ps[i])
            push!(thints, ts[i])
            i += 1
        else            # produce new cumulated point
            push!(thinps, cumpos ./ n)
            push!(thints, thints[end] + div(cumdur, n))
            cumpos, cumdur = xy(.0,.0), Dates.Millisecond(0)
            n = 0       # reconsider i in next iteration
        end
    end

    push!(thinps, ps[end])
    push!(thints, ts[end])

    #println("NOTE: thin_trace $(length(trace)) -> $(length(thinps)) positions (max_distance=$max_distance)")

    TraceXY(thinps, thints)
end
