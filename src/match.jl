# vim: set filetype=julia

# author: Laurens Devos

function _log(io::IO, msgs...)
    b = IOBuffer()
    print(b, "[", now(), "]\t")
    for msg in msgs
        print(b, msg)
    end
    m = String(take!(b))
    println(io, m)
    myid() == 1 && println(m)
end

function _write(buffer::IOBuffer, pathf::IO, logf::IO)
    _log(logf, "Writing $(buffer.size) bytes to disk")
    seekstart(buffer)
    write(pathf, buffer)
end

"""
Match all given traces and produce `nb_paths` paths. Write the results to
`output` in CBF format.
"""
function match_all(pathf::IO, mm::MapMatcher, traces::Vector{TraceXY},
                   conf::BssConfig, nb_paths::Int, logf::IO)
    N = length(traces)
    max_fails = 10

    _log(logf, "Matching $N traces with $conf")
    _log(logf, "MapMatcher: ", mm)

    try
        for (i, trace) in enumerate(traces)
            _log(logf, "############################################################")
            _log(logf, "Matching trace $i/$N, $trace")
            T = length(trace)
            toffset = 0
            attempt_count = 0
            buffer = IOBuffer()

            while true
                attempt_count += 1
                lattice = bss_lattice(mm, trace.positions, conf)
                t = lattice.err_t

                _log(logf, lattice, "\n")

                # Generate paths and write results
                if length(lattice) > 2
                    paths = get_random_paths(mm, lattice, nb_paths-1)
                    push!(paths, get_best_path(lattice))
                    paths = unique(filter(p->length(p)>1, paths))

                    if !isempty(paths)
                        _log(logf, "Buffering $(length(paths)) paths of average length $(round(Int, mean(length.(paths))))")
                        write_cbf(buffer, paths)
                    end
                end

                # Try again or move to next trace
                if lattice.status == BSS_SUCCESS
                   _log(logf, "Trace $i matched in $attempt_count attempts")
                   _write(buffer, pathf, logf)
                    break
                elseif max_fails < attempt_count
                    _log(logf, "Max fails of $max_fails reached. Moving on.")
                    break
                else
                    _log(logf, "Stuck at $(toffset+t)/$T, continuing with remaining trace")
                    trace = trace[t+1:end]
                    toffset = toffset+t
                end
            end
        end
    catch e
        _log(logf, "EXCEPTION: $e")
        Base.show_backtrace(logf, catch_stacktrace())
        rethrow(e)
    end
end

"""
Match given `traces`
and write paths and log to
    `./data/paths/{tracename}.pth.cbf`
    `./data/paths/{tracename}.log`
"""
function match_all(mm::MapMatcher, tracesname::String, traces::Vector{TraceXY},
                   conf::BssConfig, nb_paths::Int=10)
    pathsfile  = joinpath(@__DIR__, "../data/paths/", "$tracesname.pth.cbf")
    logfile    = joinpath(@__DIR__, "../data/paths/", "$tracesname.log")

    if isfile(pathsfile)
        print("Paths output file 'data/paths/$tracesname.pth.cbf' already exists. Overwrite? ")
        readline() ∉ ["yes", "y"] && return
    end

    #if isfile(pathsfile)
    #    print("Log file 'data/paths/$tracesname.log' already exists. Overwrite? ")
    #    readline() ∉ ["yes", "y"] && return
    #end

    open(pathsfile, "w") do pathf
        open(logfile, "w") do logf
            _log(logf, "Matching traces '$tracesname'")
            _log(logf, "Writing paths to '$pathsfile'")
            match_all(pathf, mm, traces, conf, nb_paths, logf)
        end
    end
end

"""
Match traces in
    `./data/traces/{tracename}.tr.cbf`
and write paths and log to
    `./data/paths/{tracename}.pth.cbf`
    `./data/paths/{tracename}.log`
"""
function match_all(mm::MapMatcher, tracesname::String, conf::BssConfig,
                   nb_paths::Int=10)
    tracesfile = joinpath(@__DIR__, "../data/traces/", "$tracesname.tr.cbf")

    if !isfile(tracesfile)
        error("Trace file 'data/traces/$tracesname.tr.cbf' does not exist")
    end

    traces = load(tracesfile)
    match_all(mm, tracesname, traces, conf, nb_paths)
end
