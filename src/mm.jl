# vim: set filetype=julia

# author: Laurens Devos
#
# Map matching

import Distributions.TruncatedNormal

struct MMSearcher
    range::Float

    xy_to_hmm_states::Vector{Vector{Int}}

    sorted_x_perm::Vector{Int}
    sorted_x::Vector{Float}
    sorted_y_perm::Vector{Int}
    sorted_y::Vector{Float}

    union_set::Set{Int}
    result_buffer1::Vector{Int}
    result_buffer2::Vector{Int}
end

function MMSearcher(xy::Vector{Point2}, hmm_states::Vector{Coord2}, range::Float)
    N = length(xy)

    xy_to_hmm_states = map(i->Int[], 1:N)
    for (i, c) in enumerate(hmm_states)
        push!(xy_to_hmm_states[c[1]], i)
        push!(xy_to_hmm_states[c[2]], i)
    end

    sorted_x_perm = sortperm(xy; by=p->p[1])
    sorted_y_perm = sortperm(xy; by=p->p[2])

    MMSearcher(range,
               xy_to_hmm_states,
               sorted_x_perm, map(i->xy[i][1], sorted_x_perm),
               sorted_y_perm, map(i->xy[i][2], sorted_y_perm),
               Set{Int}(), Int[], Int[])
end

struct MapMatcher <: HmmBase
    xy::Vector{Point2}
    hmm_states::Vector{Coord2} # hmm state k: hmm_state[k]=[i j] s.t. xy[i]->xy[j] is a way

    _searcher::MMSearcher
end

"""
Create a map matcher object for the given OSM map.

The `range` parameter allows defining the search radius of the algorithm. The
bigger the radius, the larger the number of objects that is considered around
an observation. See `best_state_seq`and `best_state_seq_nest`.

If no solution can be found, it can help to increase the `range`
parameter at the expense of the execution time.
"""
function MapMatcher(osmmap::OsmMap, range::Float64)
    osmnodes = osm_nodes(osmmap)
    osmways = osm_ways_indices(osmmap)

    # convert (lat, lon) to XY coordinates
    # rotate into xy-place with z≈radius of Earth...
    # ... then discard z-coord
    osmnodes = convert2xy(osmnodes, osm_center(osmmap))

    MapMatcher(osmnodes, osmways,
               MMSearcher(osmnodes, osmways, range))
end


# - HMM interface -------------------------------------------------------------

initial_prob(mm::MapMatcher, state::Int) = one(Prob)
transition_prob(mm::MapMatcher, s1::Int, s2::Int) = Prob(s1 == s2 || mm.hmm_states[s1][2] == mm.hmm_states[s2][1])
observation_prob(mm::MapMatcher, s::Int, o::Point2) = begin 
    p = mm.xy[mm.hmm_states[s][1]]
    q = mm.xy[mm.hmm_states[s][2]]
    dsq = distance2seg_sq(p, q, o)
    prob = _prob_of_distance(dsq)
    #@printf("Prob of (%.2f %.2f) to (%.2f, %.2f)->(%.2f, %.2f) = %s (d=%f)\n",
    #        o[1], o[2], p[1], p[2], q[1], q[2], string(prob), sqrt(dsq))
    Prob(prob)
end
states(mm::MapMatcher) = 1:size(mm.hmm_states, 1)
states_near(mm::MapMatcher, o::Point2) = states_near(mm, o, mm._searcher.range)
states_near(mm::MapMatcher, o::Point2, range::Float) = _range_search(mm, o, range)
observation_between(mm::MapMatcher, o1::Point2, o2::Point2) = begin
    if distance_sq(o1, o2) < 1.0
        o2 # not useful to add another observation, `o1` and `o2` only 1m apart
    else
        0.5 * (o1 + o2)
    end
end

# - HELPER functions ----------------------------------------------------------

@inline function _prob_of_distance(distance_sq, distribution=TruncatedNormal(0,75.0,0,Inf))::Prob
    Prob(Distributions.ccdf(distribution, sqrt(distance_sq)))
end

function _range_search(mm::MapMatcher, o::Point2, range::Float)
    s = mm._searcher
    min_distance_sq = range^2
    empty!(s.result_buffer1)
    empty!(s.result_buffer2)
    empty!(s.union_set)

    fx = searchsortedfirst(s.sorted_x, o[1] - range)
    lx = searchsortedlast( s.sorted_x, o[1] + range)
    fy = searchsortedfirst(s.sorted_y, o[2] - range)
    ly = searchsortedlast( s.sorted_y, o[2] + range)

    @inbounds for i in fx:lx
        push!(s.union_set, s.sorted_x_perm[i])
    end

    @inbounds for i in fy:ly
        k = s.sorted_y_perm[i]
        k ∈ s.union_set || continue # skip ones not in x-range
        push!(s.result_buffer1, k)
    end

    empty!(s.union_set)

    for k in s.result_buffer1
        states = s.xy_to_hmm_states[k]
        for st in states
            st ∈ s.union_set && continue # avoid duplicate hmm_states in result
            push!(s.union_set, st)

            p = mm.xy[mm.hmm_states[st][1]]
            q = mm.xy[mm.hmm_states[st][2]]
            dsq = distance2seg_sq(p, q, o)

            dsq < min_distance_sq && push!(s.result_buffer2, st)
        end
    end

    s.result_buffer2 # encapsulation issue, only valid between _range_search calls
end


# -----------------------------------------------------------------------------

function Base.show(io::IO, mm::MapMatcher)
    print(io, "MapMatcher(#states=$(size(mm.hmm_states,1)), range=$(mm._searcher.range)m)")
end

# -----------------------------------------------------------------------------

function refine_trace(mm::MapMatcher, trace::Vector{Point2}, factor::Float64=0.10)
    # For each position in the trace, look at the ways around (states_near) and
    # find the trace with the minimal length. Add enough additional points such
    # that the 'fineness' of the (local) map and the trace match.
    #
    # trace must be in coordinate space of `mm`
    trace = copy(trace)
    finetrace = Vector{Point2}()

    state_dist = s -> begin
        scoord = mm.hmm_states[s]
        p = mm.xy[scoord[1]]
        q = mm.xy[scoord[2]]
        sqrt(distance_sq(p, q))
    end

    while true
        push!(finetrace, trace[1])

        for (p, q) in zip(trace[1:end-1], trace[2:end])
            dist = sqrt(distance_sq(p, q))
            min_dist_near = mapreduce(state_dist, min, dist, states_near(mm, p))

            if dist > min_dist_near
                nt = max(3, Int(cld(factor * dist, min_dist_near)))
                ts = linspace(0.0, 1.0, nt)
                for t in ts
                    t == 0.0 && continue
                    push!(finetrace, (1-t) * p + t * q)
                end
            else
                push!(finetrace, q)
            end
        end

        print_with_color(:yellow, "ADDED $(length(finetrace) - length(trace)) ELEMENTS in iteration\n")

        # refinement has stopped
        length(finetrace) == length(trace) && return finetrace

        trace, finetrace = finetrace, trace
        empty!(finetrace)
    end
end
