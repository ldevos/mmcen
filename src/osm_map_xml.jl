# vim: set filetype=julia

# author: Laurens Devos
#
# Load OSM XML files.

if @isdefined(ZipFile)
    printstyled("LibExpat... ", color=:light_black)
    import LibExpat

    printstyled("Done\n", bold=true, color=:green)
end

mutable struct OsmActiveWay
    nodes::Vector{Int64}
    oneway::Bool
end

OsmActiveWay() = OsmActiveWay(Int64[], false)

mutable struct OsmMapBuilder
    nodes::Dict{Int64, OsmNode}
    graph::SimpleGraph{OsmNode}

    active_way::OsmActiveWay

    minlat::Float
    maxlat::Float
    minlon::Float
    maxlon::Float

    omitted_edges_count::Int64
end

function _xp_start_element(handler::LibExpat.XPStreamHandler, name::String, attrs)
    builder = handler.data::OsmMapBuilder

    if name == "node"
        id = parse(Int64, attrs["id"])
        lat = parse(Float, attrs["lat"])
        lon = parse(Float, attrs["lon"])
        builder.nodes[id] = OsmNode(id, latlon_deg(lat, lon))

    elseif name == "nd"
        push!(builder.active_way.nodes, parse(Int64, attrs["ref"]))

    elseif name == "tag" # k=oneway v=[yes|no]
        if attrs["k"] == "oneway" && attrs["v"] ∈ ["yes", "true", "1"]
            builder.active_way.oneway = true
        end

    elseif name == "bounds"
        builder.minlat = parse(Float, attrs["minlat"])
        builder.maxlat = parse(Float, attrs["maxlat"])
        builder.minlon = parse(Float, attrs["minlon"])
        builder.maxlon = parse(Float, attrs["maxlon"])
    end

    nothing
end

function _xp_end_element(handler::LibExpat.XPStreamHandler, name::String)
    builder = handler.data::OsmMapBuilder
    
    if name == "way"
        # add all edges to the graph (collected in active way)
        _add_edges_to_graph(builder)

        # reset active way
        builder.active_way = OsmActiveWay()
    end

    nothing
end

function _add_edges_to_graph(builder::OsmMapBuilder)
    graph = builder.graph
    nodes = builder.nodes
    edges = zip(builder.active_way.nodes[1:end-1],
                builder.active_way.nodes[2:end])
    oneway = builder.active_way.oneway

    for (n_id, m_id) in edges
        if !haskey(nodes, n_id) || !haskey(nodes, m_id) # way to node not in OSM data
            builder.omitted_edges_count += 1
            continue
        end

        n = nodes[n_id]
        m = nodes[m_id]

        hasedge(graph, n, m) && continue
        hasedge(graph, m, n) && continue

        hasnode(graph, n) || addnode!(graph, n)
        hasnode(graph, m) || addnode!(graph, m)

        addedge!(graph, n, m)
        oneway || addedge!(graph, m, n)
    end
end

function parse_osm(input::Union{IO, String, Cmd})
    cb = LibExpat.XPCallbacks()
    cb.start_element = _xp_start_element
    cb.end_element = _xp_end_element

    builder = OsmMapBuilder(Dict{Int64, OsmNode}(),
                            SimpleGraph{OsmNode}(),
                            OsmActiveWay(),
                            0, 0, 0, 0,
                            0)

    if typeof(input) <: IO
        #LibExpat.parseio(input, cb; data=builder)
        str = readstring(input)
        LibExpat.parse(str, cb; data=builder)
    elseif typeof(input) <: String
        str = open(input, "r") do h readstring(h) end
        LibExpat.parse(str, cb; data=builder)
    elseif typeof(input) <: Cmd
        str = readstring(input)
        LibExpat.parse(str, cb; data=builder)
    end

    if builder.omitted_edges_count != 0
        println("NOTE: number of omitted edges: ", builder.omitted_edges_count)
    end

    OsmMap(builder.graph,
           latlon_deg(builder.minlat, builder.minlon),
           latlon_deg(builder.maxlat, builder.maxlon))
end
