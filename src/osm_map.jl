# vim: set filetype=julia

# author: Laurens Devos

struct OsmNode
    id::Int64
    pos::Point3
end

struct OsmMap
    graph::SimpleGraph{OsmNode}

    minpos::Point3
    maxpos::Point3
end

function osm_center(m::OsmMap) # the center coordinate of the map
    clat = (m.maxpos[1] + m.minpos[1]) / 2.0
    clon = (m.maxpos[2] + m.minpos[2]) / 2.0

    latlon(clat, clon)
end

function osm_nodes(m::OsmMap)
    map(nodes(m.graph)) do osmnode
        osmnode.pos
    end
end

function osm_ways_indices(m::OsmMap) # indices into `osm_nodes(m)`
    edge_indices(m.graph)
end

"""
Get the adjacency matrix as a sparse matrix. Output is `Is,Js,Vs,N`, the
indices of the non-zero elements `Is,Js` and values `Vs`. The dimensions of the
matrix are `N×N`.
"""
function osm_adjacency_lists(m::OsmMap)
    outs = OsmNode[]
    gnodes = nodes(m.graph)
    gnodeindices = Dict{OsmNode, Int}(map(p->p[2]=>p[1], enumerate(gnodes)))
    Is, Js, Fs = Int[], Int[], Float64[]

    for gnode in gnodes
        i = gnodeindices[gnode]
        outgoing!(m.graph, gnode, outs)
        for outnode in outs
            j = gnodeindices[outnode]
            push!(Is, i)
            push!(Js, j)
            push!(Fs, 1.0)
        end
        empty!(outs)
    end

    Is, Js, Fs, length(gnodes)
end

"""
The sparse adjacency matrix of the OSM map.
"""
function osm_adjacency_matrix(m::OsmMap, symmetric::Bool=true)
    Is, Js, Fs, N = osm_adjacency_lists(m)
    M = sparse(Is, Js, Fs, N, N)
    if symmetric
        Msym = M + M'
        Msym[find(Msym .> 1.0)] = 1.0
        Msym
    else
        M
    end
end
