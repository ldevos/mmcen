
println("Loading Beijing...")
beij = load("data/osm/beijing-hw.osm.cbf")
mmb = MapMatcher(beij, 1000.0)
conf = BssConfig(nest_enabled=true,
                 max_frame_size=100,
                 print=BSS_PRINT_NOTHING,
                 max_runtime_ms=10000)
files = geolife_trace_files()

println("Setting up cluster...")
pids = setup_cluster()

println("Number of processes: ", nprocs())

match_all_geolife_traces_clustered(files)
