using LibExpat

include("../src/point.jl")
include("../src/graph.jl")
include("../src/osm_map.jl")
include("../src/osm_map_xml.jl")
include("testtools.jl")

osm = """
<?xml version='1.0' encoding='UTF-8'?>
<osm version="0.6" generator="fake">
  <bounds minlat="0" minlon="0" maxlat="10" maxlon="10"/>
  <node id="1" lat="1" lon="1"/>
  <node id="2" lat="2" lon="2"/>
  <node id="3" lat="3" lon="3"/>
  <node id="4" lat="4" lon="4"/>
  <node id="4" lat="4" lon="4"/>
  <node id="5" lat="5" lon="5"/>
  <node id="6" lat="6" lon="6"/>
  <node id="7" lat="7" lon="7"/>
  <node id="8" lat="8" lon="8"/>
  <node id="9" lat="9" lon="9"/>
  <way id="10">
    <nd ref="1"/>
    <nd ref="2"/>
    <nd ref="3"/>
    <nd ref="4"/>
    <nd ref="5"/>
    <nd ref="6"/>
    <tag k="highway" v="secundary"/>
    <tag k="oneway" v="no"/>
  </way>
  <way id="11">
    <nd ref="6"/>
    <nd ref="7"/>
    <nd ref="8"/>
    <nd ref="9"/>
    <tag k="highway" v="secundary"/>
    <tag k="oneway" v="yes"/>
  </way>
</osm>
"""

@suite "OSM map" begin
    m = parse_osm(IOBuffer(osm))

    @test "graph size" Base.:(==) size(m.graph) (9, 13)
    @test "min pos" Base.:(==) m.minpos latlon_deg(0, 0)
    @test "max pos" Base.:(==) m.maxpos latlon_deg(10, 10)

    ns = osm_nodes(m)
    edgs = osm_ways_indices(m)

    @test "osm_nodes" Base.:(==) all(p->latlon_deg(p[1],p[1])==p[2], enumerate(ns)) true
    @test "osm_ways_indices" Base.:(==) edgs map(Coord2, [(1, 2), (2, 1), (2, 3),
                                                          (3, 2), (3, 4), (4, 3),
                                                          (4, 5), (5, 4), (5, 6),
                                                          (6, 5), (6, 7), (7, 8),
                                                          (8, 9)])

end
