include("../src/point.jl")
include("testtools.jl")

approxf = approx(eps(Float))

@suite "Euclidian points" begin
    p1 = xy(0, 0)
    p2 = xy(3, 4)
    @test "distance2D" Base.:(==) distance_sq(p1, p2) 5^2

    p3 = xyz(0, 0, 0)
    p4 = xyz(3, 4, 5)
    @test "distance3D" approxf distance_sq(p3, p4) 50

    p = xy(-3, -1)
    q = xy(-1, -2)
    r = xy( 1, -2)
    @test "dot((-3,1),(-1,-2),(0,0))" approxf dot(p, q, p1) 5.0
    @test "dot((-3,1),( 1,-2),(0,0))" approxf dot(p, r, p1) -1.0

    @test "dist2line 1" approxf distance2line_sq(p, q, p1) 5.0
    @test "dist2line 2" approxf distance2line_sq(p, q, r) 0.8

    @test "dist2seg 1.1" approxf distance2seg_sq(p, q, p1) distance2line_sq(p, q, p1)
    @test "dist2seg 1.2" approxf distance2seg_sq(p, q, p1) distance_sq(q, p1)
    @test "dist2seg 2.1" approxf distance2seg_sq(p, q, p2) distance_sq(q, p2)
    @test "dist2seg 2.2" approxf distance2seg_sq(p, q, r) distance_sq(q, r)
    @test "dist2seg 2.3" approxf distance2seg_sq(p, q, xy(-3, 0)) 1
    @test "dist2seg 3.1" approxf distance2seg_sq(p, q, xy(-1, 0)) distance2line_sq(p, q, xy(-1, 0))

    @test "angle 1" approxf angle(xy(1,0),xy(0,1),xy(0,0)) π/2
    @test "angle 2" approxf angle(xy(1,1),xy(0,1),xy(0,0)) π/4
    @test "angle 3" approxf angle(xy(1,1),xy(-1,1),xy(0,0)) π/2
    @test "angle 4" approxf angle(xy(-1,1),xy(1,1),xy(0,0)) 3π/2
    @test "angle 5" approxf angle(xy(-1,0),xy(1,0),xy(0,0)) π
    @test "angle 6" approxf angle(xy(1,0),xy(1,0),xy(0,0)) 0.0
    @test "angle 7" approxf angle(xy(0,-10),xy(1,10),xy(0,0)) deg2rad(174.28940686250035118)
    @test "angle 8" approxf angle(xy(0,-10),xy(-1,10),xy(0,0)) 2π-deg2rad(174.28940686250035118)
end

@suite "latlon2xy(z)" begin
    f = (a,b) -> distance_sq(a,b) < eps(Float)

    @test "( 0°,90°)->(1,0,0)" f latlon2xyz(latlon_deg( 0,90), UnitPlanet()) xyz(1,0,0)
    @test "(90°, 0°)->(0,1,0)" f latlon2xyz(latlon_deg(90, 0), UnitPlanet()) xyz(0,1,0)
    @test "( 0°, 0°)->(0,0,1)" f latlon2xyz(latlon_deg( 0, 0), UnitPlanet()) xyz(0,0,1)
end
