include("testtools.jl")

struct IntHmm <: HmmBase
    nb_states::Int
    max_distance::Float64
end

initial_prob(hmm::IntHmm, s::Int) = 1 <= s <= hmm.nb_states ? one(Prob) : zero(Prob)
transition_prob(hmm::IntHmm, s::Int, t::Int) = s==t || mod(s, hmm.nb_states)+1==t ? one(Prob) : zero(Prob)
observation_prob(hmm::IntHmm, s::Int, obs::Float64) = Prob(1.0 / (1.0+(2.0*(s-obs))^2))
states(hmm::IntHmm) = 1:hmm.nb_states
states_near(hmm::IntHmm, obs::Float64) = begin
    l = min(hmm.nb_states, max(1, Int(round(obs - hmm.max_distance))))
    u = min(hmm.nb_states, max(1, Int(round(obs + hmm.max_distance))))
    l<u ? (l:u) : (u:l)
end
observation_between(hmm::IntHmm, o1::Float64, o2::Float64) = begin
    if norm(o2-o1) < 0.5
        o2
    else
        0.5 * (o1+o2)
    end
end

@suite "IntHmm" begin
    hmm = IntHmm(10, 2.0)
    nest = BssConfig(print=BSS_PRINT_NOTHING)
    nonest = BssConfig(nest_enabled=false, print=BSS_PRINT_NOTHING)
    gbp = l -> get_best_path(l)

    obs1 = [1.0, 2.2, 2.4, 2.9, 3.0, 3.1, 4.0, 4.9, 5.4, 6.6, 7.1, 8.0, 9.5, 0.9]
    @test "obs1 no nest" Base.:(==) gbp(bss_lattice(hmm, obs1, nonest)) [1,2,3,4,5,6,7,8,9,10,1]
    @test "obs1 forw.nest" Base.:(==) gbp(bss_lattice(hmm, obs1, nest)) [1,2,3,4,5,6,7,8,9,10,1]

    obs2 = [1.0, 2.2, 2.9, 3.0, 6.0, 10.0, 10.0, 1.0]
    lattice_nonest = bss_lattice(hmm, obs2, nonest)
    @test "obs2 no.nest fail" Base.:(==) lattice_nonest.status == BSS_SUCCESS false
    @test "obs2 forw.nest" Base.:(==) gbp(bss_lattice(hmm, obs2, nest)) [1,2,3,4,5,6,7,8,9,10,1]
end
