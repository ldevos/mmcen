approx(ϵ) = (result, expected) -> abs(result-expected) < ϵ

macro suite(name, body)
    return quote
        print_with_color(:bold, "Running test suite '$($(esc(name)))'\n")
        local head = @sprintf("    %-30s  %-10s %-10s %-10s\n", "[test name]", "[result]", "[expect]", "[error]")
        print_with_color(:light_black, head)
        local suitefn() = begin
            $(esc(body))
        end
        suitefn()
        println()
    end
end

macro test(name, fun, test, expected)
    return quote
        local res = $(esc(test))
        local exptd = $(esc(expected))

        if typeof(res) <: Real
            @printf(" -- %-30s % .3e % .3e % .3e ", $(esc(name)), res, exptd, exptd-res)
        else
            @printf(" -- %-30s  %-10s %-10s %-10s", $(esc(name)), res, exptd, "")
        end

        if $(esc(fun))(res, $(esc(expected)))
            print_with_color(:green, "OK")
        else
            print_with_color(:red, "FAIL")
        end
        println()
    end
end
