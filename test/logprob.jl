include("../src/logprob.jl")
include("testtools.jl")

@suite "LogProb" begin
    approxf = approx(eps(Float64))
    approxf10 = approx(eps(Float64)*10.0)
    approxf100 = approx(eps(Float64)*800.0)

    a, b = .25, .05
    alp = LogProb(a)
    blp = LogProb(b)

    @test "a"   approxf convert(Float64, alp) a
    @test "b"   approxf convert(Float64, blp) b
    @test "a+b" approxf convert(Float64, alp+blp) a+b
    @test "a+0" approxf convert(Float64, alp+zero(LogProb)) a+0.0
    @test "0+b" approxf convert(Float64, zero(LogProb)+blp) 0.0+b
    @test "a*b" approxf convert(Float64, alp*blp) a*b
    @test "b*a" approxf convert(Float64, blp*alp) a*b
    @test "a*0" approxf convert(Float64, alp*zero(LogProb)) 0.0
    @test "0*b" approxf convert(Float64, zero(LogProb)*blp) 0.0
    @test "a*1" approxf convert(Float64, alp*one(LogProb)) a
    @test "1*b" approxf convert(Float64, one(LogProb)*blp) b
    @test "a/b" approxf convert(Float64, alp/blp) a/b
    @test "b/a" approxf convert(Float64, blp/alp) b/a
    @test "a/0" Base.:(==) convert(Float64, alp/zero(LogProb)) Inf
    @test "0/b" approxf convert(Float64, zero(LogProb)/blp) 0.0
    @test "a/1" approxf convert(Float64, alp/one(LogProb)) a
    @test "1/b" approxf100 convert(Float64, one(LogProb)/blp) 1.0/b
    @test "a<b" approxf alp<blp false
    @test "a>b" approxf alp>blp true
    @test "log a" approxf log(alp) log(a)
    @test "log b" approxf log(blp) log(b)
    @test "LogProb(true)" Base.:(==) convert(LogProb, true) one(LogProb)
    @test "LogProb(false)" Base.:(==) convert(LogProb, false) zero(LogProb)

    N = 1000

    @test "rand a+b" Base.:(==) all(zip(rand(N), rand(N))) do p
        approxf10(convert(Float64, LogProb(p[1])+LogProb(p[2])), p[1]+p[2])
    end true

    @test "rand a*b" Base.:(==) all(zip(rand(N), rand(N))) do p
        approxf(convert(Float64, LogProb(p[1])*LogProb(p[2])), p[1]*p[2])
    end true

    @test "rand a/b" Base.:(==) all(zip(rand(N), rand(N))) do p
        approxf100(convert(Float64, LogProb(p[1])/LogProb(p[2])), p[1]/p[2])
    end true
end
