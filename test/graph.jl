include("../src/graph.jl")
include("testtools.jl")

struct MyData
    a_number::Int
end

@suite "Graph" begin
    g = SimpleGraph{MyData}()

    for i in 0:9 addnode!(g, MyData(i)) end
    for i in 0:9 addedge!(g, MyData(i), MyData((i+1)%10)) end

    @test "graph size"   Base.:(==) size(g) (10, 10)
    @test "hasnode"      Base.:(==) all([hasnode(g, MyData(i)) for i in 0:9]) true
    @test "hasnode fail" Base.:(==) hasnode(g, MyData(11)) false
    @test "hasedge"      Base.:(==) all([hasedge(g, MyData(i), MyData((i+1)%10)) for i in 0:9]) true
    @test "hasedge fail" Base.:(==) hasedge(g, MyData(1), MyData(11)) false

    @test "incoming" Base.:(==) incoming(g, MyData(0)) [MyData(9)]
    @test "outgoing" Base.:(==) outgoing(g, MyData(9)) [MyData(0)]

    @test "nodes" Base.:(==) nodes(g) collect(map(i -> MyData(i), 0:9))
    @test "edges" Base.:(==) edges(g) collect(map(i -> (MyData(i), MyData((i+1)%10)), 0:9))

    h = SimpleGraph{MyData}()
    mydatas = shuffle(map(i -> MyData(i), 1:10))
    edgs = []
    for i in 1:10 addnode!(h, mydatas[i]) end
    for (i,j) in zip(1:10, shuffle(1:10)) 
        addedge!(h, mydatas[i], mydatas[j])
        push!(edgs, (mydatas[i], mydatas[j]))
    end
    @test "edges2" Base.:(==) edges(h) edgs
end
