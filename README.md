# Map Matching and Matrix Functions

Code for the master thesis _'Most important locations on a road map'_ at the
KU Leuven.

## Running `mmcen`

1. Install [Julia 0.6.2](https://julialang.org/downloads/).
2. Clone this repository on your local machine (and optionally on the other
   computers that will be part of the compute cluster).
3. Install the necessary Julia packages. This can be done by running `julia
   install-packages.jl`. The Julia `Pkg` module will clone the needed packages
   onto your local machine (`$HOME/.julia/v0.6/`). Machines that will be part
   of the compute cluster will also need these packages installed locally.
4. Download trace files, these are not included in this repository.


## Functionality

To start Julia with the `mmcen` code loaded, run:

```
julia -L main.jl
```

To limit the start-up time, some functionality is not loaded by default.
Specifically, loading OSM XML, loading traces from the GeoLife ZIP file and
plotting needs to be manually enabled with the following respective macros
`@enablexml`, `@enablegeolife` and `@enableplots`.

### Reading OpenStreetMap data

Included in this repository is the Beijing map data in `cbf` format. To load
this map data, run:

```julia
beij = load("data/osm/beijing-hw.osm.cbf")
```

The `hw` letters in the name refer to the fact that this OSM map was
preprocessed such that it only contains roads. This dramatically reduces the
size and complexity of the map. The `osmium` tool can be used to perform this
preprocessing:

```
osmium tags-filter in.osm.pbf w/highway=motorway,trunk,primary,secondary,tertiary,unclassified,residential,service,motorway_link,trunk_link,primary_link,secondary_link,tertiary_link -o out.osm.pbf
```

It is also possible to read 
[OSM XML](https://wiki.openstreetmap.org/wiki/OSM_XML) and 
[OSM PBF](https://wiki.openstreetmap.org/wiki/PBF_Format) files. The PBF
reader depends on the [Osmium tool](http://osmcode.org/osmium-tool/); the
`osmium` command needs to be available. To load Beijing (or any other map),
run:

```julia
@enablexml                             # Enable the XML functionality
beij = load("path/to/beijing.osm.pbf") # PBF format, requires Osmium-tool
beij = load("path/to/beijing.osm")     # XML format, large file
```

OpenStreetMap data can be downloaded from [Planet OSM and its
mirrors](https://wiki.openstreetmap.org/wiki/Planet.osm).

### Loading traces

The experiments in this thesis are based on the __Microsoft® GeoLife__ (see
references below) data set. These traces are packaged in a ZIP file and are
organized in 182 trace groups. Each group contains a different number of actual
traces. To load a trace from the ZIP file, run:

```julia
@enablegeolife
tr = open(GeoLife, "path/to/GeoLife.zip") do gl
    gl[group_no][trace_no]
end
```

The
[`do`-syntax](https://docs.julialang.org/en/stable/manual/functions/#Do-Block-Syntax-for-Function-Arguments-1)
ensures that the GeoLife ZIP is closed after use.

The loaded trace `tr` is in the `(latitude, longitude, altitude)` format. We'll
work in meters, so we will rotate the earth around its center such that the
center of the OSM map we consider is at latitude-longitude coordinate `(0°,
0°)`. We then
[convert](https://en.wikipedia.org/wiki/Reference_ellipsoid#Coordinates) the
latitude-longitude coordinates into meters, discarding the Z-coordinate that
approximately equals the radius of Earth (assuming the size of the map
relatively small so the curvature of the Earth is negligible).

```julia
t2 = convert2xy(tr, osm_center(beij))
```

Traces are preprocessed before we try to match them to the map. Some traces
contain impossible jumps. These are removed by looking at the velocities that
are required to move from one position to the next (see `speed_jump_reduction`
and `distance_jump_reduction`). Some traces are excessively fine-grained, i.e.
the average distance between two consecutive positions is small. Positions that
lie within `max_distance` from each other are combined (see `thin_trace`). The
preprocessing can be performed with the following command:

```julia
max_distance = 50.0 # meter
t2p = preprocess(t2, max_distance)
```
To avoid having to repeat these steps every single time, there is a `CBF`
binary data format that efficiently stores preprocessed traces. These traces
can be written and read as follows:

```julia
# Note that a tr.cbf file stores a vector of traces like `t2p`.
open("path/to/tracegroup.tr.cbf", "w") do f
    write_cbf(f, [t2p])
end

t2p_cbf = load("path/to/tracegroup.tr.cbf")[1]
```

The traces are not included in this repository because I am uncertain if I am
allowed to redistribute the GeoLife data set in its original or in a modified
format.

### Map Matching

Given a map and a preprocessed trace, we can proceed and match the trace to the
map. First, we construct a `MapMatcher` object. This is a hidden Markov model
(HMM) implementation (a toy implementation of a HMM can be found in
`/test/hmm.jl`):

```julia
search_radius = 1000.0 # Radius within which streets are considered
mmb = MapMatcher(beij, search_radius)
```

To perform map matching, we first construct a lattice (see
`src/hmm.jl#bss_lattice` documentation). We can then use this lattice to
construct the one best path or multiple likely paths using Monte Carlo
simulation.

```julia
conf = BssConfig(
    nest_enabled = true,   # Set to false to disable non-emitting states (NEST)
    max_frame_size = 0,    # Set to a non-zero (i.e. 100) to enable pruning
    print = BSS_PRINT_PROGRESS # Or BSS_PRINT_DEBUG, BSS_PRINT_NOTHING
    max_runtime_ms = 10^4, # Fail after this amount of time in ms
    ratio_threshold = 1.5) # Decides when a NEST should be inserted;
                           # fine tune to limit unnecessary NESTs
lattice = bss_lattice(mmb, t2p.positions, conf)

remove_duplicates = true
path = get_best_path(lattice, remove_duplicates)

nb_of_paths = 10
paths = get_random_paths(mmb, lattice, nb_of_paths, remove_duplicates)
```

A path is represented as a vector of integers. The integers are indexes of the
*HMM states* of the used `MapMatcher` object. The states of the map matcher are
*OSM ways*, which are tuples of coordinates on the map (nodes).

A batch of traces can be matched at once using the `match_all` function. It is
also possible to divide the computation between multiple computers connected
via SSH. See [Clustering](#markdown-header-clustering).

The resulting paths can be written to disk in a CBF format using a `write_cbf`
[method](https://docs.julialang.org/en/stable/manual/methods/).

```julia
# Note that a pth.cbf file stores a vector of paths.
open("path/to/paths.pth.cbf", "w") do f
    write_cbf(f, [path]) # OR
    write_cbf(f, paths)
end

paths_cbf = load("path/to/paths.pth.cbf") # Vector{Vector{Int}}
```

### Plotting

Maps, traces and paths can be visualized using the OpenGL plotting functions:

```julia
@enableplots

p = plotnew(beij)   # Create a Beijijng plot
plot_add_trace!(p, t2, color_tuple)
plot_add_mm_states!(p, map_matcher, states, color_tuple)
plot(p)
```

Only tested on Linux and Windows machines with recent hardware.

### Clustering

As there are many traces to be map-matched, it's useful to divide up the
work amongst multiple processors. A cluster of computers can be defined in a
`./machinefile` file, which has lines of the following format:

```
# comment, line ignore, as are empty lines
<user>@<host>:<port>
```

Julia will
[connect](https://docs.julialang.org/en/stable/manual/parallel-computing/#ClusterManagers-1)
to these machines using SSH. These machines should be
accessible without password (use keys).

To then run trace preprocessing or map matching in parallel on each of the
machines defined in the `machinefile`, run the following commands (see
`./driver.jl`):

```julia
# This can take a while, quite a bit of data is exchanged over the (potentially
# slow) network (±5MB to each worker process).
setup_cluster()

# Run trace preprocessing in parallel:
export_geolife_cbf_clustered(geolife_zip, osmmap)

# Run map matching in parallel:
match_all_geolife_traces_clustered(map_matcher, bss_config, trace_files)
```


### Constructing the network matrix

We use the paths to construct a sparse matrix `A`: for each path element `e =
(i,j)`, we do `A[i,j] += 1` and `A[j,i] += A` (note that A is symmetric). This
can be done in Julia as follows:

```julia
files = ["your/paths/paths1.pth.cbf", "your/paths/paths2.ptr.cbf"]
paths = reduce(append!, Vector{Int}[], load.(files))
A = probability_matrix(mmb, paths)

# You can then iteratively compute the T largest sigular values:
S = svds(A; nsv=T)[1]

# We then have a T-rank approximation for A:
Aapprox = S.U * diagm(S.S) * S.Vt       # OutOfMemory -> this is a dense matrix

# We can compute the matrix exponential as follows:
W = diagm(S.S) * S.Vt * S.U             # T×T matrix
Wexp = expm(W)                          # compute matrix exponential on small W
Aexp = S.U * Wexp * S.U'                # OutOfMemory again, but we could look at diagonal
```

## Data references

### OpenStreetMap

The OpenStreetMap (OSM) Beijing map was used (© OpenStreetMap-authors):
[OpenStreetMap](https://www.openstreetmap.org/) under the
[ODbL](https://opendatacommons.org/licenses/odbl/). For more information,
see the [OpenStreetMap License](https://www.openstreetmap.org/copyright)
page.

### Microsoft® GeoLife

The
[Microsoft® GeoLife data set](https://www.microsoft.com/en-us/download/details.aspx?id=52367)
traces were used.

 - Yu Zheng, Lizhu Zhang, Xing Xie, Wei-Ying Ma. [Mining interesting locations and travel sequences from GPS trajectories](research.microsoft.com/apps/pubs/?id=79440). In
Proceedings of International conference on World Wild Web (WWW 2009), Madrid Spain. ACM Press: 791-800.

 - Yu Zheng, Quannan Li, Yukun Chen, Xing Xie, Wei-Ying Ma. [Understanding Mobility Based on GPS Data](http://research.microsoft.com/apps/pubs/?id=77984). In Proceedings of
ACM conference on Ubiquitous Computing (UbiComp 2008), Seoul, Korea. ACM Press: 312-321.

 - Yu Zheng, Xing Xie, Wei-Ying Ma, GeoLife: [A Collaborative Social Networking Service among User, location and trajectory](http://research.microsoft.com/apps/pubs/?id=131038).
Invited paper, in IEEE Data Engineering Bulletin. 33, 2, 2010, pp. 32-40.
